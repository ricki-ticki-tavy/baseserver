package boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"boot.config.security", "web.controller"})
public class MvcConfiguration extends WebMvcConfigurerAdapter
{

  @Override
  public void addResourceHandlers(final ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/js/**").addResourceLocations("/static/js/");
    registry.addResourceHandler("/templates/**").addResourceLocations("/static/templates/");
    registry.addResourceHandler("/img/**").addResourceLocations("/static/img/");
    registry.addResourceHandler("/css/**").addResourceLocations("/static/css/");
    registry.addResourceHandler("/webjars/bootstrap/3.3.7/css/**").addResourceLocations("/static/bootstrap-3.3.7-dist/css/");
    registry.addResourceHandler("/webjars/bootstrap/3.3.7/fonts/**").addResourceLocations("/static/bootstrap-3.3.7-dist/fonts/");
    registry.addResourceHandler("/webjars/bootstrap/3.3.7/js/**").addResourceLocations("/static/bootstrap-3.3.7-dist/js/");
    registry.addResourceHandler("/*.css/**").addResourceLocations("/css/");
  }

  @Override
  public void configureViewResolvers(ViewResolverRegistry registry) {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("WEB-INF/jsp/");
    resolver.setOrder(1);
    resolver.setSuffix(".jsp");
    resolver.setViewClass(JstlView.class);
    registry.viewResolver(resolver);
  }


  @Bean
  public MultipartResolver multipartResolver() {
    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
    multipartResolver.setMaxUploadSize(204857600); // 10MB
    multipartResolver.setMaxUploadSizePerFile(20485760); // 1MB
    return multipartResolver;
  }

}