package boot.config.security;

import core.security.CustomLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Map;

/**
 * Веб безопасность спринговая
 */
@EnableWebSecurity
@Controller
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String ROLE_ADMIN_NAME = "admin";
  public static final String ROLE_MANAGER_NAME = "manager";

  public static final String SPRING_ROLE_ADMIN_NAME = "ROLE_" + ROLE_ADMIN_NAME;
  public static final String SPRING_ROLE_MANAGER_NAME = "ROLE_" + ROLE_MANAGER_NAME;


  @Autowired
  CustomLogoutSuccessHandler customLogoutSuccessHandler;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .requiresChannel().anyRequest().requiresSecure();
    http
            .authorizeRequests()
            .antMatchers("/css/**"
                    , "/img/**"
                    , "/font/**"
                    , "/forum/**"
                    , "/static/js/**"
                    , "/index.*"
                    , "/mainMenu/**"
                    , "/topic/**"
                    , "/good404/**"
                    , "/goodViewForm/**"
                    , "/security/**").permitAll()

            .antMatchers("/api/admin/**").hasRole(ROLE_ADMIN_NAME)
            .antMatchers("/manage/**").hasRole(ROLE_MANAGER_NAME)
            .and()
            .formLogin()
            .loginPage("/login").loginProcessingUrl("/signin")
            .usernameParameter("username").passwordParameter("password")
//            .failureUrl("/login-error?error=true")
            .and().logout().permitAll().logoutSuccessHandler(customLogoutSuccessHandler)
            .and().csrf().disable();
  }

  @Bean
  public AuthenticationProvider authenticationProvider() {
    return new CustomAuthenticationProvider();
  }


//  @Bean
//  @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
//  public DefaultManageableImageCaptchaService getDefaultManageableImageCaptchaService(){
//    return new DefaultManageableImageCaptchaService();
//  }
//
  /**
   * Авторизация
   *
   * @param model
   * @return
   */
  @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
  public String processLoginForm(Map<String, Object> model) {
    return "security/login";
  }

  /**
   * Регистрация нового пользователя
   *
   * @param model
   * @return
   */
  @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
  public String processRegisterForm(Map<String, Object> model) {
    return "security/register";
  }

  /**
   * Подтверждение регистрации нового пользователя
   *
   * @param model
   * @return
   */
  @RequestMapping(value = {"/confirm"}, method = {RequestMethod.GET, RequestMethod.POST})
  public String processConfirmForm(Map<String, Object> model) {
    return "security/confirm";
  }

  /**
   * Восстановление пароля
   *
   * @param model
   * @return
   */
  @RequestMapping(value = {"/restoreUserAccess"}, method = RequestMethod.GET)
  public String processRestorePasswordForm(Map<String, Object> model) {
    return "security/restoreUserAccess";
  }

  /**
   * Восстановление пароля
   *
   * @param model
   * @return
   */
  @RequestMapping(value = {"/confirmRestoreAccountAccess"}, method = RequestMethod.GET)
  public String confirmRestoreAccountAccess(Map<String, Object> model) {
    return "security/confirmRestoreAccountAccess";
  }


}
