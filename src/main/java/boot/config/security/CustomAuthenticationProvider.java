package boot.config.security;

import core.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.List;

/**
 * Аутентификатор и Авторизатор пользователей
 */

public class CustomAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  UserDetailsService userDetailsService;

//  @Autowired
//  Core core;

  @Override
  public Authentication authenticate(Authentication authentication)
          throws AuthenticationException {
    String username = authentication.getName();
    String password = (String) authentication.getCredentials();

    UserDetails userDetails =userDetailsService.loadUserByUsername(username);
    if (userDetails == null || !userDetails.getPassword().equals(SecurityUtils.blake2(password))){
      throw new BadCredentialsException("Пользователь или пароль не верны.");
    }

    List<GrantedAuthority> grantedAuths = new ArrayList<>();
    grantedAuths.addAll(userDetails.getAuthorities());

//    // логин в логическом ядре
//    core.loginPlayer(username, ((UserDetailsServiceImpl.ExpandedUserDetails)userDetails).getDatabaseId());
//
    return new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
  }

  @Override
  public boolean supports(Class<?> arg0) {
    return true;
  }
}