package api.core.socket;

import com.google.gson.GsonBuilder;

public class WebSocketDeviceStateMessage {

  private DeviceStateMessageType messageType;
  private String messageData;

  public WebSocketDeviceStateMessage(DeviceStateMessageType type, String data){
    this.messageType = type;
    messageData = data;
  }

  public WebSocketDeviceStateMessage(DeviceStateMessageType type, Object data){
    this.messageType = type;
    messageData = new GsonBuilder().create().toJson(data);
  }

  public DeviceStateMessageType getMessageType() {
    return messageType;
  }

  public void setMessageType(DeviceStateMessageType messageType) {
    this.messageType = messageType;
  }

  public String getMessageData() {
    return messageData;
  }

  public void setMessageData(String messageData) {
    this.messageData = messageData;
  }
}
