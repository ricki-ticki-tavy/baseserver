package api.core.socket;

public enum DeviceStateMessageType{
  DEVICE_STATE_CHANGED, STATION_STATE_CHANGED, GLOBAL_STATE_CHANGED, PING
}

