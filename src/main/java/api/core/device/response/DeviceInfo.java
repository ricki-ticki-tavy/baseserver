package api.core.device.response;

import api.core.device.config.AnalogConfig;
import api.core.device.config.DeviceUiConfig;

public class DeviceInfo {
  public String name;
  public String state;

  public String deviceType;

  public String controlPin;
  public String controlType;

  public String statePin;
  public String stateType;

  public DeviceUiConfig uiConfig;

  public AnalogConfig analogConfig;
}
