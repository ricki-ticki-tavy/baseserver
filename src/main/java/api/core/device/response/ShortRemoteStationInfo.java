package api.core.device.response;

import java.util.List;

public class ShortRemoteStationInfo {
  private String name;
  private boolean isAlive;
  private List<ShortDeviceInfo> devices;

  public String getName() {
    return name;
  }

  public ShortRemoteStationInfo setName(String name) {
    this.name = name;
    return this;
  }

  public boolean isAlive() {
    return isAlive;
  }

  public ShortRemoteStationInfo setAlive(boolean alive) {
    isAlive = alive;
    return this;
  }

  public List<ShortDeviceInfo> getDevices() {
    return devices;
  }

  public ShortRemoteStationInfo setDevices(List<ShortDeviceInfo> devices) {
    this.devices = devices;
    return this;
  }
}
