package api.core.device.response;

public class ShortDeviceInfo {
  private String name;
  private String state;

  public String getName() {
    return name;
  }

  public ShortDeviceInfo setName(String name) {
    this.name = name;
    return this;
  }

  public String getState() {
    return state;
  }

  public ShortDeviceInfo setState(String state) {
    this.state = state;
    return this;
  }

}
