package api.core.device.response;

import java.util.List;

public class RemoteStationPoolInfo {
  public String gatheringStatisticIntervalMs;
  public boolean connected;
  public List<RemoteStationInfo> remoteStations;
}
