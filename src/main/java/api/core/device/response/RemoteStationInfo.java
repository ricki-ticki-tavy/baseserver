package api.core.device.response;

import api.core.device.config.RemoteStationUiConfig;

import java.util.List;

public class RemoteStationInfo {
  public String name;
  public int address;
  public boolean isAlive;
  public RemoteStationUiConfig uiConfig;
  public List<DeviceInfo> devices;
}
