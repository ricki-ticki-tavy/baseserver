package api.core.device.response;

import java.util.List;

public class ShortRemoteStationPoolInfo {
  private boolean connected;
  private List<ShortRemoteStationInfo> remoteStations;

  public boolean isConnected() {
    return connected;
  }

  public ShortRemoteStationPoolInfo setConnected(boolean connected) {
    this.connected = connected;
    return this;
  }

  public List<ShortRemoteStationInfo> getRemoteStations() {
    return remoteStations;
  }

  public ShortRemoteStationPoolInfo setRemoteStations(List<ShortRemoteStationInfo> remoteStations) {
    this.remoteStations = remoteStations;
    return this;
  }
}
