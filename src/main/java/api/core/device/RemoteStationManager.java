package api.core.device;

import api.core.Result;
import api.core.device.config.RemoteStationConfig;
import api.core.device.config.RemoteStationManagerConfig;
import api.core.device.response.RemoteStationPoolInfo;
import api.core.device.response.ShortRemoteStationPoolInfo;
import api.core.socket.DeviceStateMessageType;
import core.devices.uart.PacketLockObject;

import java.util.List;

public interface RemoteStationManager {

  Result<RemoteStation> registerStation(RemoteStationConfig stationConfig);

  Result<RemoteStation> stationByAddress(int address);

  Result<RemoteStation> stationByName(String name);

  Result<RemoteStation> unregisterStation(RemoteStation station);

  Result<PacketLockObject> doRequest(Packet packet, int timeOutMs);

  int getStationGatherStatisticsIntervalMs();

  int getStationPingTimeoutMs();

  int getServerAddress();

  List<RemoteStation> getRemoteStations();

  Result<Device> setDeviceState(String stationName, String deviceName, String state);

  <O> Result<O> getDeviceState(String stationName, String deviceName);

  RemoteStationPoolInfo toInfo();

  ShortRemoteStationPoolInfo toShortInfo();

  RemoteStationManagerConfig getConfig();

  void sendMessageToUi(DeviceStateMessageType type, Object data);

  }
