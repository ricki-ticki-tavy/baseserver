package api.core.device;

import api.core.device.config.DeviceConfig;
import api.core.device.response.DeviceInfo;
import api.core.device.response.ShortDeviceInfo;

public interface Device {
  String getName();

  DeviceConfig getConfig();
  Device setConfig(DeviceConfig config);

  <O> O getState();

  /**
   *
   * @param value
   * @return true if state has been changed
   */
  Boolean setState(String value);

  RemoteStation getParentStation();

  DeviceInfo toInfo();

  ShortDeviceInfo toShortInfo();
}
