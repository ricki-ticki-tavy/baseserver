package api.core.device;

public enum Command {
  WRITE_PIN_LOW(0),
  WRITE_PIN_HIGH(1),
  READ_PIN(2, true, byte.class),
  READ_OUTPUT_PIN_STATE(9, true, byte.class),
  READ_ANALOG(3, true, int.class),
  PING(4, true, byte.class),
  WRITE_PWM(5),
  WRITE_SERVO(6, true, byte.class),
  READ_ECHO_DISTANCE(7, true, int.class),
  DELAY(8);

  private byte code;
  private Class answerType;
  private boolean hasAnswer;

  private Command(int code, boolean hasAnswer, Class answerType) {
    this.code = (byte) code;
    this.answerType = answerType;
    this.hasAnswer = hasAnswer;
  }

  private Command(int code, boolean hasAnswer) {
    this(code, hasAnswer, null);
  }

  private Command(int code) {
    this(code, false, null);
  }

  public byte getCode() {
    return code;
  }

}
