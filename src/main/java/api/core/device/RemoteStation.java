package api.core.device;

import api.core.Result;
import api.core.device.config.DeviceConfig;
import api.core.device.config.RemoteStationConfig;
import api.core.device.response.RemoteStationInfo;
import api.core.device.response.ShortRemoteStationInfo;
import core.devices.uart.PacketLockObject;

import java.util.Date;
import java.util.List;

public interface RemoteStation {
  String getName();

  int getAddress();

  /**
   * Check if device is online
   * @return
   */
  boolean isAlive();

  /**
   * returns current alive flag
   * @return
   */
  boolean getCurrentIsAlive();

  /**
   *
   * @return
   */
  Result<Date> getLastPingTime();

  /**
   * Send request to device
   * @param request
   * @param timeoutMs
   * @return
   */
  Result<PacketLockObject> doRequest(Packet request, int timeoutMs);

  /**
   * add device to station
   * @param config
   * @return
   */
  Result<Device> registerDevice(DeviceConfig config);

  /**
   * Uregixter device
   * @param device
   * @return
   */
  Result<Device> unregisterDevice(Device device);

  /**
   * returns state of device
   * @param deviceName
   * @param <T>
   * @return
   */
  <T> Result<T> getDeviceState(String deviceName);

  /**
   *
   * @param name
   * @return
   */
  Result<Device> deviceByName(String name);

  /**
   * apply config and init all mechanisms
   * @return
   */
  Result<RemoteStation> init();

  /**
   * Read all station devices states. Call this instead of PING
   * @return
   */
  Result<RemoteStation> dispatch();

  RemoteStationConfig getConfig();

  List<Device> getDevices();

  RemoteStationInfo toInfo();

  ShortRemoteStationInfo toShortInfo();

}
