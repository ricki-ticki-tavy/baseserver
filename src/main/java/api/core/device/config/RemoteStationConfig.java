package api.core.device.config;

import java.util.List;

public class RemoteStationConfig {
  private String name;
  private int address;
  private RemoteStationUiConfig ui;
  private DeviceUiConfig defaultDeviceUi;
  private List<DeviceConfig> devices;

  public String getName() {
    return name;
  }

  public int getAddress() {
    return address;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAddress(int address) {
    this.address = address;
  }

  public List<DeviceConfig> getDevices() {
    return devices;
  }

  public void setDevices(List<DeviceConfig> devices) {
    this.devices = devices;
  }

  public RemoteStationUiConfig getUi() {
    return ui;
  }

  public void setUi(RemoteStationUiConfig ui) {
    this.ui = ui;
  }

  public DeviceUiConfig getDefaultDeviceUi() {
    return defaultDeviceUi;
  }

  public void setDefaultDeviceUi(DeviceUiConfig defaultDeviceUi) {
    this.defaultDeviceUi = defaultDeviceUi;
  }
}
