package api.core.device.config;

public enum At328AnalogPin {
  NONE(-1),
  D0(0),
  D1(1),
  D2(2),
  D3(3),
  D4(4),
  D5(5),
  D6(6),
  D7(7),
  D8(8),
  D9(9),
  D10(10),
  D11(11),
  D13(13),
  A0(14),
  A1(15),
  A2(16),
  A3(17),
  A4(18),
  A5(19),
  A6(20),
  A7(21);

  private byte code;

  private At328AnalogPin(int code) {
    this.code = (byte) code;
  }

  public byte getCode() {
    return code;
  }

}
