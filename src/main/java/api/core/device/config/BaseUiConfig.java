package api.core.device.config;

public abstract class BaseUiConfig {

  private String commonStyle;
  private String noConnStyle;
  private String onStyle;
  private String offStyle;

  public String getCommonStyle() {
    return commonStyle;
  }

  public void setCommonStyle(String commonStyle) {
    this.commonStyle = commonStyle;
  }

  public String getNoConnStyle() {
    return noConnStyle;
  }

  public void setNoConnStyle(String noConnStyle) {
    this.noConnStyle = noConnStyle;
  }

  public String getOnStyle() {
    return onStyle;
  }

  public void setOnStyle(String onStyle) {
    this.onStyle = onStyle;
  }

  public String getOffStyle() {
    return offStyle;
  }

  public void setOffStyle(String offStyle) {
    this.offStyle = offStyle;
  }

}
