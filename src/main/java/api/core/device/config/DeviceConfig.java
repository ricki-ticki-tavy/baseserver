package api.core.device.config;

public class DeviceConfig {
  private String name;
  private DeviceClass deviceClass;
  private At328AnalogPin controlPin;
  private At328AnalogPin statePin;
  private int pauseToSwitchStateMs;
  private DeviceUiConfig ui;
  private AnalogConfig analogConfig;

  public String getName() {
    return name;
  }

  public DeviceClass getDeviceClass() {
    return deviceClass;
  }

  public At328AnalogPin getControlPin() {
    return controlPin;
  }

  public At328AnalogPin getStatePin() {
    return statePin;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDeviceClass(DeviceClass deviceClass) {
    this.deviceClass = deviceClass;
  }

  public void setControlPin(At328AnalogPin controlPin) {
    this.controlPin = controlPin;
  }

  public void setStatePin(At328AnalogPin statePin) {
    this.statePin = statePin;
  }

  public int getPauseToSwitchStateMs() {
    return pauseToSwitchStateMs;
  }

  public DeviceUiConfig getUi() {
    return ui;
  }

  public void setUi(DeviceUiConfig ui) {
    this.ui = ui;
  }

  public void setPauseToSwitchStateMs(int pauseToSwitchStateMs) {
    this.pauseToSwitchStateMs = pauseToSwitchStateMs;
  }

  public AnalogConfig getAnalogConfig() {
    return analogConfig;
  }

  public void setAnalogConfig(AnalogConfig analogConfig) {
    this.analogConfig = analogConfig;
  }
}
