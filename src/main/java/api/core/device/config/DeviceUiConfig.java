package api.core.device.config;

public class DeviceUiConfig extends BaseUiConfig{

  private double stateMultiplier;

  public double getStateMultiplier() {
    return stateMultiplier;
  }

  public void setStateMultiplier(double stateMultiplier) {
    this.stateMultiplier = stateMultiplier;
  }
}
