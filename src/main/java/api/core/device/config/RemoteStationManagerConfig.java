package api.core.device.config;

import java.util.List;

public class RemoteStationManagerConfig {
  private List<RemoteStationConfig> stations;
  private int gatherStatisticsIntervalMs;
  private int gatherStatisticsTimeoutMs;
  private int setDeviceStateTimeoutMs;

  public List<RemoteStationConfig> getStations() {
    return stations;
  }

  public void setStations(List<RemoteStationConfig> stations) {
    this.stations = stations;
  }

  public int getGatherStatisticsIntervalMs() {
    return gatherStatisticsIntervalMs;
  }

  public void setGatherStatisticsIntervalMs(int gatherStatisticsIntervalMs) {
    this.gatherStatisticsIntervalMs = gatherStatisticsIntervalMs;
  }

  public int getGatherStatisticsTimeoutMs() {
    return gatherStatisticsTimeoutMs;
  }

  public void setGatherStatisticsTimeoutMs(int gatherStatisticsTimeoutMs) {
    this.gatherStatisticsTimeoutMs = gatherStatisticsTimeoutMs;
  }

  public int getSetDeviceStateTimeoutMs() {
    return setDeviceStateTimeoutMs;
  }

  public void setSetDeviceStateTimeoutMs(int setDeviceStateTimeoutMs) {
    this.setDeviceStateTimeoutMs = setDeviceStateTimeoutMs;
  }
}
