package api.core.device.config;

public enum DeviceManageType {
  NONE, DIGITAL, ANALOG, SERVO
}
