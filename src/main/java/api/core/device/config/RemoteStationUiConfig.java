package api.core.device.config;

public class RemoteStationUiConfig extends BaseUiConfig{

  private boolean noUi = false;

  public boolean isNoUi() {
    return noUi;
  }

  public void setNoUi(boolean noUi) {
    this.noUi = noUi;
  }
}
