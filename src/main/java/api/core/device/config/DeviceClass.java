package api.core.device.config;

public enum DeviceClass {
  SIMPLE_SWITCHER("Простой выключатель без обратной связи", DeviceStateType.NONE, DeviceManageType.DIGITAL),
  SWITCHER("Выключатель с обратной связью", DeviceStateType.DIGITAL, DeviceManageType.DIGITAL),
  EXTENDED_SWITCHER("Проходной выключатель с обратной связью", DeviceStateType.DIGITAL, DeviceManageType.DIGITAL),
  REGULATOR("Регулятор", DeviceStateType.NONE, DeviceManageType.ANALOG),
  BASE_PROBE("Простой датчик", DeviceStateType.ANALOG, DeviceManageType.NONE);

  private String name;

  /**
   * we can get current device state
   */
  private DeviceStateType stateType;

  /**
   * we can manage this device. Thermoprobe can't be managed
   */
  private DeviceManageType manageType;

  public String getName() {
    return name;
  }

  public DeviceStateType getStateType() {
    return stateType;
  }

  public DeviceManageType getManageType() {
    return manageType;
  }

  private DeviceClass(String name, DeviceStateType stateType, DeviceManageType manageType){
    this.name = name;
    this.stateType = stateType;
    this.manageType = manageType;
  }

}
