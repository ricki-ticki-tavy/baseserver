package api.core.device.config;

public class AnalogConfig {
  private double multiplier;
  private double zeroValue;
  private Double minWarnLevel;
  private Double maxWarLevel;
  private String measurement;
  private int accuracy;

  public double getMultiplier() {
    return multiplier;
  }

  public void setMultiplier(double multiplier) {
    this.multiplier = multiplier;
  }

  public double getZeroValue() {
    return zeroValue;
  }

  public void setZeroValue(double zeroValue) {
    this.zeroValue = zeroValue;
  }

  public Double getMinWarnLevel() {
    return minWarnLevel;
  }

  public void setMinWarnLevel(Double minWarnLevel) {
    this.minWarnLevel = minWarnLevel;
  }

  public Double getMaxWarLevel() {
    return maxWarLevel;
  }

  public void setMaxWarLevel(Double maxWarLevel) {
    this.maxWarLevel = maxWarLevel;
  }

  public String getMeasurement() {
    return measurement;
  }

  public void setMeasurement(String measurement) {
    this.measurement = measurement;
  }

  public int getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(int accuracy) {
    this.accuracy = accuracy;
  }
}
