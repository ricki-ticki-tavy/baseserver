package api.core.device.config;

public enum DeviceStateType {
  NONE, DIGITAL, ANALOG, SONIC
}
