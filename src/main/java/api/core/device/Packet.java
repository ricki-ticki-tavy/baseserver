package api.core.device;

import api.core.Result;
import core.system.ResultImpl;
import core.system.error.SystemError;
import core.system.error.SystemErrors;
import core.devices.uart.CheckSum;

import java.util.Arrays;

public class Packet {
  public static final int PACKET_FLAG_MASTER_TO_SLAVE = 7;
  public static final int PACKET_FLAG_REQUEST = 6;
  public static final byte PACKET_LENGTH_MASK = 0x3f;
  public static final byte PACKET_FLAGS_MASK = (byte) (PACKET_LENGTH_MASK ^ 0xff);

  public static final int PACKET_LENGTH_DATA_MAX = 44;
  public static final int PACKET_LENGTH_HEADER = 8;
  public static final int PACKET_LENGTH_MAX = PACKET_LENGTH_DATA_MAX + PACKET_LENGTH_HEADER;

  private byte lengthAndFlag;
  private byte xorCheckSum;
  private byte addCheckSum;
  private int senderAddr;
  private int recipientAddr;
  private byte id;
  private byte data[] = new byte[PACKET_LENGTH_DATA_MAX];
  private int dataLength = 0;
  private boolean request = true;
  private boolean masterToSlave = true;

  public Packet() {
  }

  public Packet(int recipientAddr, int senderAddr, boolean request, boolean masterToSlave) {
    setSenderAddr(senderAddr);
    setRecipientAddr(recipientAddr);
    setLengthAndFlag((byte) 0);
    setRequest(request);
    setMasterToSlave(masterToSlave);
  }
  //------------------------------------------------------------------------------------------

  public Packet(int recipientAddr, boolean request, boolean masterToSlave) {
    this(recipientAddr, 0, request, masterToSlave);
  }
  //------------------------------------------------------------------------------------------

  public Packet(int recipientAddr) {
    this(recipientAddr, true, true);
  }
  //------------------------------------------------------------------------------------------


  public byte getLengthAndFlag() {
    return lengthAndFlag;
  }

  public Packet setLengthAndFlag(byte lengthAndFlag) {
    this.lengthAndFlag = lengthAndFlag;
    return this;
  }

  public byte getXorCheckSum() {
    return xorCheckSum;
  }

  public Packet setXorCheckSum(byte xorCheckSum) {
    this.xorCheckSum = xorCheckSum;
    return this;
  }

  public byte getAddCheckSum() {
    return addCheckSum;
  }

  public Packet setAddCheckSum(byte addCheckSum) {
    this.addCheckSum = addCheckSum;
    return this;
  }

  public int getSenderAddr() {
    return senderAddr;
  }

  public Packet setSenderAddr(int senderAddr) {
    this.senderAddr = senderAddr;
    return this;
  }

  public int getRecipientAddr() {
    return recipientAddr;
  }

  public Packet setRecipientAddrHigh(int recipientAddr) {
    this.recipientAddr = recipientAddr;
    return this;
  }

  public byte getId() {
    return id;
  }

  public Packet setId(byte id) {
    this.id = id;
    return this;
  }

  public byte[] getData() {
    return data;
  }

  public Packet setData(byte[] data) {
    this.data = data;
    return this;
  }

  public Packet setRecipientAddr(int addr) {
    recipientAddr = addr;
    return this;
  }

  public Packet setRecipientAddr(byte hi, byte lo) {
    recipientAddr = hi;
    recipientAddr = (recipientAddr << 8) | lo;
    return this;
  }

  public Packet setSenderAddr(byte hi, byte lo) {
    senderAddr = hi;
    senderAddr = (senderAddr << 8) | lo;
    return this;
  }

  public byte getRecipientAddrHigh() {
    return (byte) ((recipientAddr >> 8) & 0xff);
  }

  public byte getRecipientAddrLo() {
    return (byte) (recipientAddr & 0xff);
  }

  public byte getSenderAddrHigh() {
    return (byte) ((senderAddr >> 8) & 0xff);
  }

  public byte getSenderAddrLo() {
    return (byte) (senderAddr & 0xff);
  }

  public boolean isRequest() {
    return request;
  }

  public Packet setRequest(boolean request) {
    this.request = request;
    return this;
  }

  public boolean isMasterToSlave() {
    return masterToSlave;
  }

  public Packet setMasterToSlave(boolean masterToSlave) {
    this.masterToSlave = masterToSlave;
    return this;
  }

  //------------------------------------------------------------------------------------------

  public CheckSum calcCheckSum() {
    CheckSum checkSum = new CheckSum();

    int addCs = lengthAndFlag + getRecipientAddrHigh() + getRecipientAddrLo()
            + getSenderAddrHigh() + getSenderAddrLo()
            + id;

    int xorCs = lengthAndFlag ^ getRecipientAddrHigh() ^ getRecipientAddrLo()
            ^ getSenderAddrHigh() ^ getSenderAddrLo()
            ^ id;

    for (int i = 0; i < dataLength; i++) {
      xorCs ^= data[i];
      addCs += data[i];
    }

    checkSum.addCs = (byte) (addCs & 0xff);
    checkSum.xorCs = (byte) (xorCs & 0xff);

    return checkSum;
  }
  //------------------------------------------------------------------------------------------

  public SystemError check(boolean masterToSlave, boolean isRequest) {
    // check length
    int length = lengthAndFlag & PACKET_LENGTH_MASK;

    if (length > PACKET_LENGTH_MAX) {
      return SystemErrors.PACKET_BAD_LENGTH_DESCRIPTOR.getError("" + length);
    }

//    if (length - PACKET_LENGTH_HEADER != data.length) {
//      return SystemErrors.PACKET_BAD_LENGTH.getError("" + (length - PACKET_LENGTH_HEADER), "" + data.length);
//    }
//
//    // check flag
//    if ((lengthAndFlag & PACKET_FLAGS_MASK) >> PACKET_FLAG_MASTER_TO_SLAVE > 0){
//      return SystemErrors.PACKET_BAD_MASTER_TO_SLAVE.getError("0", "1");
//    }
//

    // check check sums
    CheckSum checkSum = calcCheckSum();
    if (checkSum.addCs != addCheckSum) {
      return SystemErrors.PACKET_BAD_ADD_CHECKSUM.getError("" + addCheckSum, "" + checkSum.addCs);
    }
    if (checkSum.xorCs != xorCheckSum) {
      return SystemErrors.PACKET_BAD_XOR_CHECKSUM.getError("" + xorCheckSum, "" + checkSum.xorCs);
    }

    return null;

  }
  //------------------------------------------------------------------------------------------

  public Result<Packet> addCommandArgByte(Command command, byte args) {
    byte tmpData[] = new byte[1];
    tmpData[0] = args;
    return addCommandBytes(command, tmpData, 1);
  }
  //------------------------------------------------------------------------------------------

  public Result<Packet> addCommandArg2Bytes(Command command, byte args1, byte args2) {
    byte tmpData[] = new byte[2];
    tmpData[0] = args1;
    tmpData[1] = args2;
    return addCommandBytes(command, tmpData, 2);
  }
  //------------------------------------------------------------------------------------------

  public Result<Packet> addCommandArgInt(Command command, int args) {
    byte tmpData[] = new byte[2];
    tmpData[0] = (byte)(args & 0xff);
    tmpData[1] = (byte)((args >> 8) & 0xff);
    return addCommandBytes(command, tmpData, 2);
  }
  //------------------------------------------------------------------------------------------

  public Result<Packet> addCommandBytes(Command command, byte[] args, int argLength) {
    // check for new length
    if ((argLength + dataLength + 1) > PACKET_LENGTH_DATA_MAX) {
      return ResultImpl.fail(SystemErrors.PACKET_DATA_OVERHEAD.getError("" + PACKET_LENGTH_DATA_MAX, "" + argLength + dataLength + 1));
    }

    // copy command
    data[dataLength++] = command.getCode();

    // copy arguments
    for (int i = 0; i < argLength; i++) {
      data[dataLength++] = args[i];
    }

    // save new length
    return ResultImpl.success(this);
  }
  //------------------------------------------------------------------------------------------

  public Result<Packet> compose() {
    lengthAndFlag = (byte) (dataLength + PACKET_LENGTH_HEADER);
    if (request) {
      lengthAndFlag = (byte) (lengthAndFlag | (byte) (1 << PACKET_FLAG_MASTER_TO_SLAVE));
    }

    if (masterToSlave) {
      lengthAndFlag = (byte) (lengthAndFlag | (byte) (1 << PACKET_FLAG_REQUEST));
    }

    // calc check sums
    CheckSum checkSum = calcCheckSum();
    xorCheckSum = checkSum.xorCs;
    addCheckSum = checkSum.addCs;


    return ResultImpl.success(this);
  }
  //------------------------------------------------------------------------------------------

  public static Packet fromBytes(byte inputData[], int len) {
    Packet packet = new Packet();

    packet.setLengthAndFlag(inputData[0]);

    int dataLen = packet.lengthAndFlag & PACKET_LENGTH_MASK;
    dataLen = dataLen > len - PACKET_LENGTH_HEADER
            ? len - PACKET_LENGTH_HEADER
            : dataLen;
    dataLen = dataLen > PACKET_LENGTH_DATA_MAX
            ? PACKET_LENGTH_DATA_MAX
            : dataLen;

    packet.setXorCheckSum(inputData[1])
            .setAddCheckSum(inputData[2])
            .setSenderAddr(inputData[3], inputData[4])
            .setRecipientAddr(inputData[5], inputData[6])
            .setId(inputData[7])
            .setData(Arrays.copyOfRange(inputData, 8, dataLen))
            .setRequest(((1 << PACKET_FLAG_REQUEST) & packet.getLengthAndFlag()) > 0)
            .setMasterToSlave(((1 << PACKET_FLAG_MASTER_TO_SLAVE) & packet.getLengthAndFlag()) > 0);
    packet.dataLength = dataLen;
    return packet;
  }
  //------------------------------------------------------------------------------------------

  public byte[] getBytes() {
    byte outputBuffer[] = new byte[PACKET_LENGTH_HEADER + dataLength];

    outputBuffer[0] = lengthAndFlag;
    outputBuffer[1] = xorCheckSum;
    outputBuffer[2] = addCheckSum;

    outputBuffer[3] = getSenderAddrHigh();
    outputBuffer[4] = getSenderAddrLo();

    outputBuffer[5] = getRecipientAddrHigh();
    outputBuffer[6] = getRecipientAddrLo();

    outputBuffer[7] = id;

    for (int i = 0; i < dataLength; i++) {
      outputBuffer[PACKET_LENGTH_HEADER + i] = data[i];
    }

    return outputBuffer;
  }
  //------------------------------------------------------------------------------------------
}
