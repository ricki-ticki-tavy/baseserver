package api.core;

import java.util.Map;

/**
 * Сервис отправки или получения почты
 */
public interface MailService {

  /**
   * Отправить сообщение по электронной почте
   * @param recipient
   * @param subject
   * @param body
   * @return
   */
  Result<String> sendMail(String recipient, String subject, String body);

  /**
   * Отправить сообщение по электронной почте с применением шаблона. Для подстановки переменных в шаблон
   * использовать @params
   *
   * @param recipient     получатель письма
   * @param subject       тема письма
   * @param templateName  имя шаблона в ресурсах
   * @param params        Параметры для подстановки в шаблон. Пары ИМЯ_ПЕРЕМЕННОЙ - ЗНАЧЕНИЕ
   * @return
   */
  Result<String> sendTemplateMail(String recipient, String subject, String templateName, Map<String, String> params);
}
