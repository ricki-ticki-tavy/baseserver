package api.core;

import core.system.error.SystemError;
import core.system.error.SystemErrors;
import org.slf4j.Logger;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Класс результата действия
 */
public interface Result<T> {

  boolean isFail();

  boolean isFail(SystemErrors error);

  boolean isSuccess();

  SystemError getError();

  T getResult();

  <O> O map(Function<T, O> consumer);

  <O> Result<O> chain(Function<T, Result<O>> consumer);

  /**
   * выполняет с перехватом и оборачиванием исключений в ошибку
   * @param consumer
   * @param <O>
   * @return
   */
  <O> Result<O> mapSafe(Function<T, Result<O>> consumer);

  Result<T> peak(Consumer<T> consumer);

  @Deprecated
  <O> Result<T> doIfFail(Consumer<O> consumer);

  <O> Result<O> mapFail(Function<Result<T>, Result<O>> consumer);

  /**
   * Записать ошибку в лог, если таковая была и вернуть самого себя
   * @param logger
   * @return
   */
  Result<T> logIfError(Logger logger);
}
