package core.devices.uart;

import api.core.Result;
import api.core.device.Packet;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestsStore {
  public static final int EVICT_TIME_OUT_MS = 60000;
  Logger LOGGER = LoggerFactory.getLogger(RequestsStore.class);


  private Map<Integer, PacketLockObject> activeRequests = new ConcurrentHashMap();

  public Result<PacketLockObject> waitForResponse(Packet request, int timeOutMs, OutputStream outputFlow) throws IOException {
    // check if packet exists
    PacketLockObject lockObject = activeRequests.get(request.getId());
    if (lockObject != null) {
      if (new Date().getTime() - lockObject.created.getTime() > EVICT_TIME_OUT_MS) {
        // old request
        activeRequests.remove(request.getId());
      } else {
        return ResultImpl.fail(SystemErrors.PACKET_DUPLICATE_REQUEST.getError("" + request.getId()));
      }
    }

    lockObject = PacketLockObject.newPacketLockObject(request);
    activeRequests.put((int) request.getId(), lockObject);

    outputFlow.write(request.getBytes());
    outputFlow.flush();

    Result<PacketLockObject> result;
    try {
      result = lockObject.waitFor(timeOutMs);
    } catch (Throwable th) {
      result = ResultImpl.fail(SystemErrors.PACKET_SEND_REQUEST_UNKNOWN_ERROR.getError(th.getLocalizedMessage()));
    } finally {
      activeRequests.remove((int) request.getId());
    }

    return result;
  }
  //-----------------------------------------------------------------------

  private void evictDispatch() {
    long now = new Date().getTime();
    AtomicInteger lostRequestsCounter = new AtomicInteger(0);
    activeRequests.keySet().stream()
            .forEach(id -> {
              PacketLockObject lockObject = activeRequests.get(id);
              if (lockObject.evictAfterDate <= now) {
                activeRequests.remove(id);
                // unlock potential lock
//                try{
//                  lockObject.lock.lock();
//                  lockObject.condition.signal();
//                } finally {
//                  lockObject.lock.unlock();
//                }
                lostRequestsCounter.getAndIncrement();
              }
            });

    if (lostRequestsCounter.get() > 0) {
      LOGGER.warn("removed " + lostRequestsCounter.get() + " lost requests.");
    }
  }
  //-----------------------------------------------------------------------

  public Result<PacketLockObject> responseReceived(Packet response) {
    evictDispatch();
    // check if request with same id exists
    PacketLockObject lockObject = activeRequests.get((int) response.getId());
    if (lockObject == null) {
      return ResultImpl.fail(SystemErrors.PACKET_RECEIVE_WITH_INVALID_ID.getError("" + response.getId()));
    }

    lockObject.response = response;
    lockObject.lock.lock();
    lockObject.condition.signal();
    lockObject.lock.unlock();

    return ResultImpl.success(lockObject);
  }

}
