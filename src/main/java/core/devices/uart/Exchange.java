package core.devices.uart;

import api.core.Result;
import api.core.device.Command;
import api.core.device.Packet;
import api.core.device.config.At328AnalogPin;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortIOException;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class Exchange {

  Logger LOGGER = LoggerFactory.getLogger(Exchange.class);

  public static final int PACKET_RECEIVE_BUFFER_TIMEOUT_MS = 300;
  public static final int REQUEST_TIMEOUT_MS = 5000;

  RequestsStore requestsStore;

  int reqIdSequence = 0;

  private SerialPort port;
  private InputStream inputFlow;
  private OutputStream outputFlow;

  private byte[] uartBuffer;
  private int uartBufferPtr;
  private long uartLastRead;
  private AtomicBoolean uartConnected = new AtomicBoolean(false);
  private int uartSpeed = 9600;

  private Lock requestLock = new ReentrantLock();
  private Lock connectLock = new ReentrantLock();

  @Value("${core.lora.port:}")
  private String loraPortName;

  @Value("${core.lora.address:8001}")
  private int loraAddress;


  public Exchange() {
    requestsStore = new RequestsStore();
  }

  public Exchange(String portName, int speed, int loraAddress) {
    requestsStore = new RequestsStore();
    this.loraPortName = portName;
    this.uartSpeed = speed;
    connect(portName, speed);
    this.loraAddress = loraAddress;
  }

  void disconnect(){
    try{
      try {inputFlow.reset();} catch (Throwable th){}
      try {inputFlow.reset();} catch (Throwable th){}
      try {outputFlow.flush();} catch (Throwable th){}
      try {inputFlow.close();} catch (Throwable th){}
      try {outputFlow.close();} catch (Throwable th){}
      try {port.closePort();} catch (Throwable th){}
      uartConnected.set(Boolean.FALSE);
    } catch (Throwable th){

    }
  }

  void connect(String portName, int speed) {
    if (!uartConnected.get()) {
      connectLock.lock();
      try {
        if (!uartConnected.get()) {
          try {

            if ((portName == null || portName.isEmpty()) && SerialPort.getCommPorts().length == 1) {
              portName = SerialPort.getCommPorts()[0].getSystemPortName();
            }

            final String portNameL = portName;

            LOGGER.warn("connecting to " + portNameL);

            port = Arrays.stream(SerialPort.getCommPorts())
                    .filter(serialPort -> serialPort.getSystemPortName().equalsIgnoreCase(portNameL))
                    .findFirst()
                    .orElseThrow(() -> {
                      throw new RuntimeException("port not found");
                    });
          } catch (Throwable th) {
            LOGGER.error("Port finding error.", th);
            inputFlow = null;
            outputFlow = null;
            return;
          }

          LOGGER.warn("port exists. Opening...");

          port.setComPortParameters(speed, 8, 1, 0);
          port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING
                  | SerialPort.TIMEOUT_WRITE_BLOCKING, 100, 100);
          port.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);

          try {
            if (!port.openPort(500, 58, 58)) {
              LOGGER.error("Can't open port: " + portName);
            }
          } catch (Throwable th) {
            LOGGER.error("Can't open port " + portName, th);
            return;
          }

          inputFlow = port.getInputStream();
          outputFlow = port.getOutputStream();
          try {
            inputFlow.reset();
          } catch (Throwable th){

          }
          LOGGER.warn("Port opened: " + portName);

          uartConnected.set(true);
        }
      } finally {
        connectLock.unlock();
      }
    }
  }
  //--------------------------------------------------------

  public void resetInputBuffer() {
    uartBufferPtr = 0;
    uartLastRead = new Date().getTime();
  }
  //--------------------------------------------------------

  public void clearUartBuffer() {
    if (!uartConnected.get()) {
      return;
    }

    byte data[] = new byte[1];
    try {
      while (inputFlow.available() > 0) {
        inputFlow.read(data, 0, 1);
      }

      resetInputBuffer();
    } catch (Throwable th) {

    }
  }
  //--------------------------------------------------------

  public Result<Packet> beginRequest(int recipientAddr) {
    return ResultImpl.success(new Packet(recipientAddr, loraAddress, true, true));
  }
  //--------------------------------------------------------

  public int getLoraAddress(){
    return loraAddress;
  }
  //--------------------------------------------------------

  public Result<PacketLockObject> doRequest(Packet request, int timeoutMs) {

    requestLock.lock();
    try {
//      try { Thread.sleep(100);} catch (Throwable th){}
      if (!uartConnected.get()) {
        connect(loraPortName, uartSpeed);
        if (!uartConnected.get()) {
          return ResultImpl.fail(SystemErrors.UART_PORT_NOT_CONNECTED.getError(loraPortName));
        }
      }

      request.setId((byte) (reqIdSequence++ & 0xff));
      request.setSenderAddr(loraAddress);
      request.compose();
      try {
        return requestsStore.waitForResponse(request, timeoutMs == 0 ? REQUEST_TIMEOUT_MS : timeoutMs, outputFlow);
      } catch (SerialPortIOException spe) {
        disconnect();
        return ResultImpl.fail(SystemErrors.UART_PORT_NOT_CONNECTED.getError(loraPortName));
      } catch (IOException ioe) {
        return ResultImpl.fail(SystemErrors.UART_PORT_OUTPUT_ERROR.getError(ioe.getLocalizedMessage()));
      }
    } finally {
      requestLock.unlock();
    }
  }
  //--------------------------------------------------------


  /** */
  public void runReader() {
    new Thread(() -> {
      uartBuffer = new byte[Packet.PACKET_LENGTH_MAX];
      uartBufferPtr = 0;
      uartLastRead = new Date().getTime();
      int packetLen = 0;

      while (!Thread.currentThread().isInterrupted()) {
        try {
          if (!uartConnected.get()) {
            try{Thread.sleep(2000);} catch (Throwable th){}
            connect(loraPortName, uartSpeed);
          } else {

            if (inputFlow.available() > 0) {
              // data presents
              long current = new Date().getTime();

              if (current - uartLastRead > PACKET_RECEIVE_BUFFER_TIMEOUT_MS) {
                // new data fow. Need to reset buffer
                resetInputBuffer();
              }

              if (uartBufferPtr == 0) {
                // this is packet descriptor
                inputFlow.read(uartBuffer, uartBufferPtr++, 1);

                packetLen = (uartBuffer[0] & Packet.PACKET_LENGTH_MASK);
                if (packetLen > Packet.PACKET_LENGTH_MAX) {
                  // bad descriptor
                  clearUartBuffer();
                  continue;
                }

              }

              while (inputFlow.available() > 0) {
                inputFlow.read(uartBuffer, uartBufferPtr++, 1);
                if (packetLen == uartBufferPtr) {
                  // Packet read. check request flag
                  uartBufferPtr = 0;
                  Packet packet = Packet.fromBytes(uartBuffer, packetLen + Packet.PACKET_LENGTH_HEADER);
                  if (!packet.isRequest()) {
                    requestsStore.responseReceived(packet);
                  } else {

                  }
                }
              }

            }
            Thread.sleep(1);
          }
        } catch (SerialPortIOException spe) {
          LOGGER.error("Reconnect needed", spe);
          disconnect();
        } catch (Throwable e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  @PostConstruct
  public void onPostConstruct(){
    connect(loraPortName, 9600);
    runReader();
  }

  public void runTestThread() {
    new Thread(() -> {
      while (!Thread.currentThread().isInterrupted()) {
        Packet packet = beginRequest(0x0101).getResult();
        packet.addCommandArgByte(Command.WRITE_PIN_HIGH, (byte) 11);
        packet.addCommandArgByte(Command.DELAY, (byte) 40);
        packet.addCommandArgByte(Command.WRITE_PIN_LOW, (byte) 11);
        packet.addCommandArgByte(Command.READ_ANALOG, At328AnalogPin.A1.getCode());
        System.out.print("Sending ... ");

        Result<PacketLockObject> objectResult = doRequest(packet, 2000);
        objectResult
                .peak(packetLockObject ->
                        System.out.println(" success (" + (((packetLockObject.response.getData()[1] & 0xff) << 8) + (int) packetLockObject.response.getData()[2])))
//                        System.out.println(" success ("))
                .mapFail(packetLockObjectResult -> {

                          System.out.println(packetLockObjectResult.getError().getMessage());
                          return null;
                        }

                );
        try {
          Thread.sleep(3000);
        } catch (Throwable th) {

        }
      }
    }).start();
  }
  //----------------------------------------------------------------------------------------------------

  public void lock(){
    requestLock.lock();
  }
  //----------------------------------------------------------------------------------------------------

  public void unlock(){
    requestLock.unlock();
  }
  //----------------------------------------------------------------------------------------------------

  public boolean isConnected(){
    return uartConnected.get();
  }

}
