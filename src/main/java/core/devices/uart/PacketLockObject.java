package core.devices.uart;

import api.core.Result;
import api.core.device.Packet;
import core.system.ResultImpl;
import core.system.error.SystemErrors;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PacketLockObject {
  public Packet request;
  public Packet response;
  public Date created;
  public long evictAfterDate;
  public Lock lock;
  public Condition condition;

  public static PacketLockObject newPacketLockObject(Packet packet){
    PacketLockObject packetLockObject = new PacketLockObject();
    packetLockObject.request = packet;
    packetLockObject.response = null;
    packetLockObject.lock = new ReentrantLock();
    packetLockObject.condition = packetLockObject.lock.newCondition();
    packetLockObject.created = new Date();
    return packetLockObject;
  }

  public Result<PacketLockObject> waitFor(int timeoutMs){
    lock.lock();
    evictAfterDate = new Date().getTime() + timeoutMs;
    try{
      if (!condition.await(timeoutMs, TimeUnit.MILLISECONDS)){
        return ResultImpl.fail(SystemErrors.PACKET_REQUEST_TIMEOUT.getError("" + timeoutMs));
      } else {
        return ResultImpl.success(this);
      }
    } catch (Throwable th){
      return ResultImpl.fail(SystemErrors.PACKET_REQUEST_TIMEOUT.getError("" + timeoutMs));
    } finally {
      lock.unlock();
    }
  }

  public void unlock(Packet response){
    this.response = response;
    lock.lock();
    condition.signal();
    lock.unlock();
  }
}
