package core.devices;

import api.core.Result;
import api.core.device.*;
import api.core.device.config.RemoteStationConfig;
import api.core.device.config.RemoteStationManagerConfig;
import api.core.device.response.RemoteStationPoolInfo;
import api.core.device.response.ShortRemoteStationPoolInfo;
import api.core.socket.DeviceStateMessageType;
import core.devices.uart.Exchange;
import core.devices.uart.PacketLockObject;
import core.system.ResultImpl;
import core.system.error.SystemError;
import core.system.error.SystemErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;
import web.socket.WebSocketsController;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Component
public class RemoteStationManagerImpl implements RemoteStationManager {

  Logger LOGGER = LoggerFactory.getLogger(RemoteStationManagerImpl.class);

  private Map<String, RemoteStation> stationByName = new ConcurrentHashMap<>();
  private Map<Integer, RemoteStation> stationByAddress = new ConcurrentHashMap<>();
  private Lock mapLock = new ReentrantLock();

  RemoteStationManagerConfig config;

  @Autowired
  Exchange exchange;

  @Autowired
  SwitcherUtil switcherUtil;

  @Autowired
  WebSocketsController webSocketsController;

  @Override
  public Result<RemoteStation> registerStation(RemoteStationConfig stationConfig) {
    mapLock.lock();
    try {
      if (stationByAddress.get(stationConfig.getAddress()) != null) {
        return ResultImpl.fail(SystemErrors.STATION_DUPLICATE_ADDRESS.getError("" + stationConfig.getAddress()));
      }
      if (stationByName.get(stationConfig.getName()) != null) {
        return ResultImpl.fail(SystemErrors.STATION_DUPLICATE_NAME.getError(stationConfig.getName()));
      }

      RemoteStation remoteStation = new RemoteStationImpl(this, stationConfig);
      remoteStation.init().doIfFail(error -> {
                SystemError fineError = (SystemError) error;
                LOGGER.error(fineError.getMessage());
              }
      );

      LOGGER.info("remote station " + stationConfig.getName() + " brought up");

      stationByAddress.put(stationConfig.getAddress(), remoteStation);
      stationByName.put(stationConfig.getName(), remoteStation);

      return ResultImpl.success(remoteStation);

    } finally {
      mapLock.unlock();
    }
  }
  //--------------------------------------------------------------------------


  @Override
  public int getStationGatherStatisticsIntervalMs() {
    return config.getGatherStatisticsIntervalMs();
  }
  //--------------------------------------------------------------------------

  @Override
  public int getStationPingTimeoutMs() {
    return config.getGatherStatisticsTimeoutMs();
  }
  //--------------------------------------------------------------------------

  @Override
  public Result<RemoteStation> stationByAddress(int address) {
    RemoteStation device = stationByAddress.get(address);
    return device == null
            ? ResultImpl.fail(SystemErrors.STATION_FIND_BY_NAME_ERROR.getError("" + address))
            : ResultImpl.success(device);
  }
  //--------------------------------------------------------------------------

  @Override
  public Result<RemoteStation> stationByName(String name) {
    RemoteStation device = stationByName.get(name);
    return device == null
            ? ResultImpl.fail(SystemErrors.STATION_FIND_BY_NAME_ERROR.getError(name))
            : ResultImpl.success(device);
  }
  //--------------------------------------------------------------------------

  @Override
  public Result<RemoteStation> unregisterStation(RemoteStation station) {
    mapLock.lock();
    try {
      RemoteStation foundStation = stationByName.get(station.getName());
      if (foundStation != null) {
        stationByName.remove(foundStation.getName());
        stationByAddress.remove(foundStation.getAddress());
        return ResultImpl.success(foundStation);
      }
      return ResultImpl.fail(SystemErrors.STATION_FIND_BY_ID_ERROR.getError("" + station.getAddress()));
    } finally {
      mapLock.unlock();
    }
  }
  //--------------------------------------------------------------------------

  @Override
  public Result<PacketLockObject> doRequest(Packet packet, int timeOutMs) {
    return exchange.doRequest(packet, timeOutMs);
  }
  //--------------------------------------------------------------------------

  @Override
  public int getServerAddress() {
    return exchange.getLoraAddress();
  }
  //--------------------------------------------------------------------------

  @Override
  public List<RemoteStation> getRemoteStations() {
    return new ArrayList<>(stationByName.values());
  }
  //--------------------------------------------------------------------------

  private void applyConfig() {
    LOGGER.info("device configuration applying...");
    config.getStations().stream()
            .forEach(stationConfig ->
                    registerStation(stationConfig))
    ;
    LOGGER.info("device configuration applied");
  }
  //--------------------------------------------------------------------------

  @PostConstruct
  private void runManager() {
    try {
      Yaml yaml = new Yaml();
      InputStream inputYaml = yaml.getClass().getClassLoader().getResourceAsStream("devices.yaml");
      if (inputYaml != null) {
        config = yaml.load(inputYaml);
      } else {
        throw new RuntimeException("file 'devices.yaml' not found");
      }
      LOGGER.info("device configuration loaded");
      applyConfig();
      initTimeSend();
    } catch (Throwable th) {
      LOGGER.error("Error load device configuration", th);
    }
    runSystemThread();
  }
  //--------------------------------------------------------------------------

  public void initTimeSend() {
    new Thread(() -> {
      try {Thread.sleep(2000);} catch (Throwable th) {}
      while (!Thread.interrupted()) {
        try {
          Thread.sleep(1000);
          webSocketsController.sendMessage(DeviceStateMessageType.PING, toShortInfo());
        } catch (Throwable th) {
          LOGGER.error("send PING error ", th);
        }
      }
    }).start();
  }

  //--------------------------------------------------------------------------

  private void runSystemThread() {
    new Thread(() -> {
      while (!Thread.currentThread().isInterrupted()) {
        try {
          Thread.sleep(1000);
          stationByAddress.values().stream()
                  .forEach(station -> {
                    // need ti ping station if the last ping was earlier than MAX_INTERVAL
                    if (station.getLastPingTime().isFail() || new Date().getTime() - station.getLastPingTime().getResult().getTime() - 1 > getStationGatherStatisticsIntervalMs()) {
                      // need ping
                      LOGGER.info("gathering statistic from station '" + station.getName() + "' (address " + station.getAddress() + ")");
                      boolean currentIsAlive = station.getCurrentIsAlive();
                      boolean newIsAlive = station.isAlive();
                      if (currentIsAlive && !newIsAlive) {
                        LOGGER.warn("station '" + station.getName() + "' (address " + station.getAddress() + ") goes to OFF LINE");
                      } else if (!currentIsAlive && newIsAlive) {
                        LOGGER.warn("station '" + station.getName() + "' (address " + station.getAddress() + ") brought to ON LINE");
                      }
                    }
                  });
        } catch (Throwable th) {
          LOGGER.error("SYSTEM THREAD ", th);
        }
      }
    }).start();
    LOGGER.info("Starting system service.");
  }
  //--------------------------------------------------------------------------

  @Override
  public <O> Result<O> getDeviceState(String stationName, String deviceName) {
    return stationByName(stationName)
            .chain(remoteStation -> remoteStation.deviceByName(deviceName))
            .chain(device -> ResultImpl.success(device.getState()));
  }
  //--------------------------------------------------------------------------

  @Override
  public Result<Device> setDeviceState(String stationName, String deviceName, String state) {
    return stationByName(stationName)
            .chain(remoteStation -> (Result<RemoteStation>) (remoteStation.isAlive()
                    ? ResultImpl.success(remoteStation)
                    : ResultImpl.fail(SystemErrors.STATION_IS_OFF_LINE.getError(remoteStation.getConfig().getName()
                    , "" + remoteStation.getConfig().getAddress()))))
            .chain(remoteStation -> remoteStation.deviceByName(deviceName))
            .chain(device -> {
              Result result = null;
              Packet request;
              // prepare request for set new value
              switch (device.getConfig().getDeviceClass()) {
                case SIMPLE_SWITCHER:
                  // request current state
                  request = new Packet(device.getParentStation().getAddress());
                  request.addCommandArgByte(Boolean.valueOf(state)
                                  ? Command.WRITE_PIN_HIGH
                                  : Command.WRITE_PIN_LOW
                          , device.getConfig().getControlPin().getCode());
                  result = doRequest(request, config.getSetDeviceStateTimeoutMs());
                  break;
                case SWITCHER:
                  result = switcherUtil.setSwitcherState(device, Boolean.valueOf(state));
                  break;
                case EXTENDED_SWITCHER:
                  result = switcherUtil.setExtendedSwitcherState(device, Boolean.valueOf(state));
                  break;
                case BASE_PROBE:
                  result = ResultImpl.fail(SystemErrors.DEVICE_IS_NOT_MANAGED.getError(deviceName, stationName));
              }

              Result outpuResult = result.peak(packetLockObject -> device.setState(state))
                      .chain(packetLockObject -> ResultImpl.success(device));

              return outpuResult;
            });
  }
  //--------------------------------------------------------------------------

  @Override
  public RemoteStationPoolInfo toInfo() {
    RemoteStationPoolInfo info = new RemoteStationPoolInfo();
    info.connected = exchange.isConnected();
    info.remoteStations = stationByName.values().stream()
            .map(remoteStation -> remoteStation.toInfo())
            .collect(Collectors.toList());
    return info;
  }
  //--------------------------------------------------------------------------

  @Override
  public ShortRemoteStationPoolInfo toShortInfo() {
    return new ShortRemoteStationPoolInfo()
            .setConnected(exchange.isConnected())
            .setRemoteStations(stationByName.values().stream()
                    .map(remoteStation -> remoteStation.toShortInfo())
                    .collect(Collectors.toList()));
  }
  //--------------------------------------------------------------------------

  @Override
  public RemoteStationManagerConfig getConfig() {
    return config;
  }
  //--------------------------------------------------------------------------

  public void sendMessageToUi(DeviceStateMessageType type, Object data) {
    webSocketsController.sendMessage(type, data);
  }
}
