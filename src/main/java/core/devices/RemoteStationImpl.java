package core.devices;

import api.core.Result;
import api.core.device.*;
import api.core.device.config.At328AnalogPin;
import api.core.device.config.DeviceConfig;
import api.core.device.config.DeviceStateType;
import api.core.device.config.RemoteStationConfig;
import api.core.device.response.RemoteStationInfo;
import api.core.device.response.ShortRemoteStationInfo;
import api.core.socket.DeviceStateMessageType;
import core.devices.uart.PacketLockObject;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class RemoteStationImpl implements RemoteStation {

  private final Logger LOGGER = LoggerFactory.getLogger(RemoteStationImpl.class);
  private RemoteStationConfig config;
  private RemoteStationManager remoteStationsManager;
  private Date lastPingTime = null;
  private boolean alive = false;

  private Map<String, Device> devicesByName = new ConcurrentHashMap<>();
  private Map<At328AnalogPin, Device> devicesByPin = new ConcurrentHashMap<>();

  public RemoteStationImpl(RemoteStationManager remoteStationsManager, RemoteStationConfig config) {
    this.config = config;
    this.remoteStationsManager = remoteStationsManager;
  }

  @Override
  public Result<RemoteStation> init() {
    LOGGER.info("Registering remote station '" + config.getName() + "'");
    return config.getDevices().stream()
            .map(deviceConfig ->
                    registerDevice(deviceConfig)
                            .peak(device -> {
                              LOGGER.info("   device '" + deviceConfig.getName() + "' has been registered");
                              if (StringUtils.isEmpty(device.getConfig().getUi().getNoConnStyle())) {
                                device.getConfig().getUi().setNoConnStyle(config.getDefaultDeviceUi().getNoConnStyle());
                              }
                              if (StringUtils.isEmpty(device.getConfig().getUi().getOffStyle())) {
                                device.getConfig().getUi().setOffStyle(config.getDefaultDeviceUi().getOffStyle());
                              }
                              if (StringUtils.isEmpty(device.getConfig().getUi().getOnStyle())) {
                                device.getConfig().getUi().setOnStyle(config.getDefaultDeviceUi().getOnStyle());
                              }
                            }))
            .filter(deviceResult -> deviceResult.isFail())
            .findFirst()
            .orElse(ResultImpl.success(this));
  }

  @Override
  public String getName() {
    return config.getName();
  }

  @Override
  public int getAddress() {
    return config.getAddress();
  }

  @Override
  public Result<Date> getLastPingTime() {
    return lastPingTime == null
            ? ResultImpl.fail(SystemErrors.STATION_HAS_NO_SUCCESS_PING.getError(getName(), "" + getAddress())) :
            ResultImpl.success(lastPingTime);
  }
  //-------------------------------------------------------------------------

  @Override
  public boolean isAlive() {
    if (lastPingTime == null || (new Date().getTime() - lastPingTime.getTime() > remoteStationsManager.getStationGatherStatisticsIntervalMs())) {
      lastPingTime = new Date();
      dispatch()
              .peak(remoteStation -> alive = true)
              .mapFail(remoteStation -> {
                alive = false;
                return ResultImpl.fail(SystemErrors.STATION_REQUEST_ERROR.getError(getName(), "" + getAddress()));
              });
    }
    return alive;
  }
  //-------------------------------------------------------------------------

  @Override
  public boolean getCurrentIsAlive() {
    return alive;
  }
  //-------------------------------------------------------------------------

  @Override
  public Result<PacketLockObject> doRequest(Packet request, int timeoutMs) {

    return remoteStationsManager.doRequest(request, timeoutMs);
  }
  //-------------------------------------------------------------------------

  /**
   * NOT THREAD SAFE
   *
   * @param config
   * @return
   */
  @Override
  public Result<Device> registerDevice(DeviceConfig config) {
    // check for duplicate name
    if (devicesByName.get(config.getName()) != null) {
      return ResultImpl.fail(SystemErrors.DEVICE_DUPLICATE_NAME.getError(config.getName(), this.config.getName()));
    }
    // check for pin duplicate
    if (config.getControlPin() != At328AnalogPin.NONE && devicesByPin.get(config.getControlPin()) != null) {
      return ResultImpl.fail(SystemErrors.DEVICE_DUPLICATE_PIN.getError(devicesByPin.get(config.getControlPin()).getName()
              , this.config.getName()
              , "" + config.getControlPin()
              , config.getName()));
    }
    if (config.getStatePin() != At328AnalogPin.NONE && devicesByPin.get(config.getStatePin()) != null) {
      return ResultImpl.fail(SystemErrors.DEVICE_DUPLICATE_PIN.getError(devicesByPin.get(config.getStatePin()).getName()
              , this.config.getName()
              , "" + config.getStatePin()
              , config.getName()));
    }

    Device device = new DeviceImpl(this, config);
    devicesByName.put(device.getName(), device);
    if (config.getStatePin() != At328AnalogPin.NONE) {
      devicesByPin.put(config.getStatePin(), device);
    }
    if (config.getControlPin() != At328AnalogPin.NONE) {
      devicesByPin.put(config.getControlPin(), device);
    }

    return ResultImpl.success(device);
  }
  //-------------------------------------------------------------------------

  @Override
  public Result<Device> unregisterDevice(Device device) {
    Device foundDevice = devicesByName.values().stream()
            .filter(device1 -> device1 == device)
            .findFirst()
            .orElse(null);
    if (foundDevice == null) {
      return ResultImpl.fail(SystemErrors.DEVICE_NOT_REGISTERED.getError(device.getName(), config.getName()));
    }

    devicesByName.remove(foundDevice.getName());
    devicesByPin.remove(foundDevice.getConfig().getStatePin());
    devicesByPin.remove(foundDevice.getConfig().getControlPin());

    return ResultImpl.success(foundDevice);
  }
  //-------------------------------------------------------------------------

  @Override
  public Result<Device> deviceByName(String name) {
    Device device = devicesByName.get(name);
    return device == null
            ? ResultImpl.fail(SystemErrors.DEVICE_NOT_REGISTERED.getError(name, config.getName()))
            : ResultImpl.success(device);
  }
  //-------------------------------------------------------------------------

  @Override
  public <T> Result<T> getDeviceState(String deviceName) {
    return deviceByName(deviceName)
            .chain(device -> device.getState());
  }
  //-------------------------------------------------------------------------

  @Override
  public List<Device> getDevices() {
    return new ArrayList<>(devicesByName.values());
  }
  //-------------------------------------------------------------------------

  @Override
  public RemoteStationConfig getConfig() {
    return config;
  }
  //-------------------------------------------------------------------------

  @Override
  public Result<RemoteStation> dispatch() {
    // collect all devices that has state
    List<Device> deviceList = devicesByName.values().stream()
            .filter(device -> device.getConfig().getStatePin() != At328AnalogPin.NONE)
            .collect(Collectors.toList());

    Packet request = new Packet(this.config.getAddress());
    deviceList.stream()
            .forEach(device -> {

              switch (device.getConfig().getDeviceClass()) {
                case SIMPLE_SWITCHER:
                  request.addCommandArgByte(Command.READ_OUTPUT_PIN_STATE, device.getConfig().getControlPin().getCode());
                  break;
                case EXTENDED_SWITCHER:
                case SWITCHER:
                case BASE_PROBE:
                  switch (device.getConfig().getDeviceClass().getStateType()) {
                    case DIGITAL:
                      request.addCommandArgByte(Command.READ_PIN, device.getConfig().getStatePin().getCode());
                      break;
                    case SONIC:
                      request.addCommandArgByte(Command.READ_ECHO_DISTANCE, device.getConfig().getStatePin().getCode());
                      break;
                    case ANALOG:
                      request.addCommandArgByte(Command.READ_ANALOG, device.getConfig().getStatePin().getCode());
                      break;
                  }
                  break;
              }
            });

    Result<PacketLockObject> objectResult = remoteStationsManager.doRequest(request, 2000)
            .peak(packetLockObject -> {
              // parse response
              AtomicInteger dataPtr = new AtomicInteger(0);
              byte data[] = packetLockObject.response.getData();
              deviceList.stream()
                      .forEach(device -> {
                        switch (device.getConfig().getDeviceClass()) {
                          case SIMPLE_SWITCHER:
                            if (data[dataPtr.getAndIncrement()] != Command.READ_PIN.getCode()) {
                            } // skip command
                            device.setState(Boolean.valueOf(data[dataPtr.getAndIncrement()] != 0).toString());
                            break;
                          case SWITCHER:
                          case EXTENDED_SWITCHER:
                          case BASE_PROBE:
                            switch (device.getConfig().getDeviceClass().getStateType()) {
                              case DIGITAL:
                                if (data[dataPtr.getAndIncrement()] != Command.READ_PIN.getCode()) {
                                } // skip command
                                device.setState(Boolean.valueOf(data[dataPtr.getAndIncrement()] != 0).toString());
                                break;
                              case SONIC:
                                if (data[dataPtr.getAndIncrement()] != Command.READ_ECHO_DISTANCE.getCode()) {
                                } // skip command
                                break;
                              case ANALOG:
                                if (data[dataPtr.getAndIncrement()] != Command.READ_ANALOG.getCode()) {
                                } // skip command
                                device.setState(Integer.valueOf((int)(data[dataPtr.getAndIncrement()] & 0xff) + ((int)(data[dataPtr.getAndIncrement()] & 0xff) << 8)).toString());
                                break;
                            }
                            break;
                        }
                      });
            });

    return objectResult.isSuccess()
            ? ResultImpl.success(this)
            : ResultImpl.fail(objectResult.getError());
  }
  //-------------------------------------------------------------------------

  //  @Override
//  public Result<RemoteStation> dispatch() {
//    // collect all devices that has state
//    List<Device> deviceList = devicesByName.values().stream()
//            .filter(device -> device.getConfig().getStatePin() != At328AnalogPin.NONE)
//            .collect(Collectors.toList());
//
//    Packet request = new Packet(this.config.getAddress());
//    deviceList.stream()
//            .forEach(device -> {
//              switch (device.getConfig().getDeviceClass().getStateType()) {
//                case DIGITAL:
//                  request.addCommandArgByte(Command.READ_PIN, device.getConfig().getStatePin().getCode());
//                  break;
//                case SONIC:
//                  request.addCommandArgByte(Command.READ_ECHO_DISTANCE, device.getConfig().getStatePin().getCode());
//                  break;
//                case ANALOG:
//                  request.addCommandArgByte(Command.READ_ANALOG, device.getConfig().getStatePin().getCode());
//                  break;
//              }
//            });
//
//    Result<PacketLockObject> objectResult = remoteStationsManager.doRequest(request, 2000)
//            .peak(packetLockObject -> {
//              // parse response
//              AtomicInteger dataPtr = new AtomicInteger(0);
//              byte data[] = packetLockObject.response.getData();
//              deviceList.stream()
//                      .forEach(device -> {
//                        switch (device.getConfig().getDeviceClass().getStateType()) {
//                          case DIGITAL:
//                            if (data[dataPtr.getAndIncrement()] != Command.READ_PIN.getCode()) {
//                            } // skip command
//                            device.setState(Boolean.valueOf(data[dataPtr.getAndIncrement()] != 0).toString());
//                            break;
//                          case SONIC:
//                            if (data[dataPtr.getAndIncrement()] != Command.READ_ECHO_DISTANCE.getCode()) {
//                            } // skip command
//                            break;
//                          case ANALOG:
//                            if (data[dataPtr.getAndIncrement()] != Command.READ_ANALOG.getCode()) {
//                            } // skip command
//                            device.setState(Integer.valueOf(data[dataPtr.getAndIncrement()] + (data[dataPtr.getAndIncrement()] << 8)).toString());
//                            break;
//                        }
//
//                      });
//            });
//
//    return objectResult.isSuccess()
//            ? ResultImpl.success(this)
//            : ResultImpl.fail(objectResult.getError());
//  }
//  //-------------------------------------------------------------------------
//
  @Override
  public RemoteStationInfo toInfo() {
    RemoteStationInfo info = new RemoteStationInfo();
    info.address = config.getAddress();
    info.isAlive = alive;
    info.name = config.getName();
    info.uiConfig = config.getUi();
    info.devices = devicesByName.values().stream()
            .map(device -> device.toInfo())
            .collect(Collectors.toList());
    return info;
  }
  //-------------------------------------------------------------------------

  @Override
  public ShortRemoteStationInfo toShortInfo() {
    return new ShortRemoteStationInfo()
            .setAlive(alive)
            .setName(getName())
            .setDevices(devicesByName.values().stream()
                    .map(device -> device.toShortInfo())
                    .collect(Collectors.toList()));
  }
  //-------------------------------------------------------------------------
}
