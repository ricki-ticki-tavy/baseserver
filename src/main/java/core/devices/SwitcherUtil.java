package core.devices;

import api.core.Result;
import api.core.device.Command;
import api.core.device.Device;
import api.core.device.Packet;
import api.core.device.RemoteStationManager;
import core.devices.uart.Exchange;
import core.system.ResultImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SwitcherUtil {

  @Autowired
  RemoteStationManager remoteStationManager;

  @Autowired
  Exchange exchange;

  public Result<Device> setSwitcherState(Device device, boolean newValue) {
    try {
      exchange.lock();
      Packet request = new Packet(device.getParentStation().getAddress());
      return remoteStationManager.doRequest(request, remoteStationManager.getConfig().getSetDeviceStateTimeoutMs())
              .peak(packetLockObject -> {
                device.setState(newValue + "");
              })
              .map(packetLockObject -> ResultImpl.success(device));
    } finally {
      exchange.unlock();
    }
  }

  public Result<Device> setExtendedSwitcherState(Device device, boolean newValue) {
    try {
      exchange.lock();
      Packet request1 = new Packet(device.getParentStation().getAddress());
      // request current control pin state
      request1.addCommandArgByte(Command.READ_PIN, device.getConfig().getStatePin().getCode());
      // read current state value
      request1.addCommandArgByte(Command.READ_OUTPUT_PIN_STATE, device.getConfig().getControlPin().getCode());
      return remoteStationManager.doRequest(request1, remoteStationManager.getConfig().getSetDeviceStateTimeoutMs())
              .chain(packetLockObject -> {
                if ((packetLockObject.response.getData()[1] != 0) != newValue) {
                  // new state and current are differs
                  // answer for second command is in byte 3 of data. Second command request current control pin state
                  boolean controlPinValue = packetLockObject.response.getData()[3] != 0;

                  Packet request = new Packet(device.getParentStation().getAddress());

                  // change control pin state
                  request.addCommandArgByte(Boolean.valueOf(!controlPinValue)
                                  ? Command.WRITE_PIN_HIGH
                                  : Command.WRITE_PIN_LOW
                          , device.getConfig().getControlPin().getCode());

                  // delay for switcher turns on or off
                  request.addCommandArgInt(Command.DELAY, device.getConfig().getPauseToSwitchStateMs());

                  // read control pin state
                  request.addCommandArgByte(Command.READ_OUTPUT_PIN_STATE, device.getConfig().getControlPin().getCode());

                  // read status pin state
                  request.addCommandArgByte(Command.READ_PIN, device.getConfig().getStatePin().getCode());

                  return remoteStationManager.doRequest(request, device.getConfig().getPauseToSwitchStateMs() + remoteStationManager.getConfig().getSetDeviceStateTimeoutMs())
                          .peak(packetLockObject1 -> {
                            if (packetLockObject1.response != null) {

                            }
                          });
                } else {
                  return ResultImpl.success(device);
                }
              })
              .chain(packetLockObject -> ResultImpl.success(device));
    } finally {
      exchange.unlock();
    }

  }
}
//--------------------------------------------------------------------------



