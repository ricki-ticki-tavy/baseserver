package core.devices;

import api.core.device.Device;
import api.core.device.RemoteStation;
import api.core.device.config.DeviceConfig;
import api.core.device.response.DeviceInfo;
import api.core.device.response.ShortDeviceInfo;

public class DeviceImpl implements Device {

  private DeviceConfig deviceConfig;
  private RemoteStation parentRemoteStation;
  private Object state;

  public DeviceImpl(RemoteStation parentRemoteStation, DeviceConfig config) {
    this.deviceConfig = config;
    this.parentRemoteStation = parentRemoteStation;
    state = null;
  }

  @Override
  public RemoteStation getParentStation() {
    return parentRemoteStation;
  }

  @Override
  public String getName() {
    return deviceConfig.getName();
  }

  @Override
  public DeviceConfig getConfig() {
    return deviceConfig;
  }

  @Override
  public Device setConfig(DeviceConfig config) {
    this.deviceConfig = config;
    return this;
  }

  @Override
  public <O> O getState() {
    return (O) state;
  }

  @Override
  public Boolean setState(String newState) {
    boolean changed = false;
    switch (deviceConfig.getDeviceClass()){
      case SIMPLE_SWITCHER:
        changed = this.state != Boolean.valueOf(newState);
        this.state = Boolean.valueOf(newState);
        break;
      case EXTENDED_SWITCHER:
      case SWITCHER:
        switch (deviceConfig.getDeviceClass().getStateType()) {
          case DIGITAL:
            changed = this.state != Boolean.valueOf(newState);
            this.state = Boolean.valueOf(newState);
            break;
          case ANALOG:
          case SONIC:
            changed = this.state != Integer.valueOf(newState);
            this.state = Integer.valueOf(newState);
        }
        break;

      case BASE_PROBE:
        switch (deviceConfig.getDeviceClass().getStateType()) {
          case DIGITAL:
            changed = this.state != Boolean.valueOf(newState);
            this.state = Boolean.valueOf(newState);
            break;
          case ANALOG:
          case SONIC:
            changed = this.state != Integer.valueOf(newState);
            if (deviceConfig.getAnalogConfig() != null){
              this.state = Integer.valueOf(newState) * deviceConfig.getAnalogConfig().getMultiplier() - deviceConfig.getAnalogConfig().getZeroValue();
            } else {
              this.state = Integer.valueOf(newState);
            }
        }

        break;

    }
    return changed;
  }
  //------------------------------------------------------------------------

  @Override
  public DeviceInfo toInfo() {
    DeviceInfo info = new DeviceInfo();
    info.controlPin = deviceConfig.getControlPin().toString();
    info.controlType = deviceConfig.getDeviceClass().getManageType().toString();
    info.statePin = deviceConfig.getStatePin().toString();
    info.stateType = deviceConfig.getDeviceClass().getStateType().toString();

    info.state = state == null ? "NULL" : state.toString();
    info.name = deviceConfig.getName();
    info.deviceType = deviceConfig.getDeviceClass().toString();
    info.uiConfig = deviceConfig.getUi();
    info.analogConfig = deviceConfig.getAnalogConfig();
    return info;
  }
  //------------------------------------------------------------------------

  @Override
  public ShortDeviceInfo toShortInfo() {
    return new ShortDeviceInfo().
    setName(deviceConfig.getName())
    .setState(state == null ? "NULL" : state.toString());
  }
  //------------------------------------------------------------------------
}
