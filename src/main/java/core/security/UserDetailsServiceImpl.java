package core.security;

import boot.config.security.SecurityConfig;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserDetailsServiceImpl implements UserDetailsService {

  private static final String GET_USER_BY_NAME = "from User where name = :name";

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    List<GrantedAuthority> auth = new ArrayList<>();
    auth.add(new SimpleGrantedAuthority(SecurityConfig.ROLE_ADMIN_NAME));
    auth.add(new SimpleGrantedAuthority(SecurityConfig.ROLE_MANAGER_NAME));

    org.springframework.security.core.userdetails.User user =
            new org.springframework.security.core.userdetails.User(
                    username,
                    "admin",
                    true,
                    true,
                    true,
                    true
                    , auth);
    return user;
  }
}
