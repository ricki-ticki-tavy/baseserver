package core.system.error;

public enum SystemErrors {

   USER_UNKNOWN_NAME("U-81", "Пользователь с именем %s не найден.")

  , SYSTEM_NOT_REALIZED("SE-1", "Метод %s не реализован.")
  , SYSTEM_USER_ERROR("SE-2", "%s.")
  , SYSTEM_RUNTIME_ERROR("SE-3", "%s.")
  , SYSTEM_OBJECT_ALREADY_INITIALIZED("SE-4", "Объект %s уже инициализирован (%s). Повторное переопределение значения невозможно.")
  , SYSTEM_BAD_PARAMETER("SE-5", "Недопустимое значение параметра %s (%s). %s")
  , SYSTEM_BAD_PARAMETERS("SE-6", "Недопустимый набор параметров. %s")
  , SYSTEM_NOT_AUTHORIZED("SE-7", "Пользователь не авторизован")

  , USER_PASSWORDS_NOT_EQUALS("US-1", "Пароли пусты или не совпадают")
  , USER_EMAIL_ALREADY_USED("US-2", "Адрес электронной почты '%s' уже используется")
  , USER_EMAIL_IS_EMPTY("US-3", "Адрес электронной почты не задан")
  , USER_NAME_IS_EMPTY("US-4", "Не задано имя пользователя")
  , USER_NAME_ALREADY_USED("US-5", "Пользователь '%s' уже существует")
  , USER_CONFIRM_CODE_IS_OUTDATED("US-6", "Ссылка не верна, либо срок действия истек")
  , USER_RESTORE_ACCOUNT_FIELDS_ARE_EMPTY("US-7", "Долно быть заполнено или поле электронной почты или поле логина")
  , USER_RESTORE_ACCOUNT_DOES_NOT_EXISTS("US-8", "Пользователь не найден")
  , USER_BAD_CAPTCHA("US-9", "В сессии нет проверочного кода")
  , USER_CAPTCHA_NOT_EQUIVALENT("US-10", "Проверочный код не совпал")

  , EMAIL_SEND_ERROR("EM-1", "Ошибка отправки письма: %s")
  , EMAIL_RECEIVE_ERROR("EM-2", "Ошибка отправки письма: %s")
  , EMAIL_LETTER_TEMPLATE_READ_ERROR("EM-3", "Не удалось загрузить шаблон письма '%s")


   , IMAGE_NOT_FOUND_BY_ID("IM-1", "Изображение с кодом '%s' не найдено")
   , IMAGE_BAD_REQUEST_BY_ID("IM-2", "Изображение с кодом '%s' недопустимо для скачивания")
   , IMAGE_BAD_RESOLUTION("IM-3", "Недопустимое разрешение '%s'")
   , IMAGE_BAD_DIMENTION_VALUE("IM-4", "Недопустимое значение '%s' : '%s")

  , UART_PORT_NOT_CONNECTED("UE-0", "Port '%s' not opened")
  , UART_PORT_NOT_FOUND("UE-1", "Port '%s' not found")
  , UART_PORT_OPEN_ERROR("UE-2", "Error open port '%s': %s")
  , UART_PORT_OUTPUT_ERROR("UE-3", "Error while writing to port: '%s'")

  , PACKET_BAD_LENGTH_DESCRIPTOR("UP-0", "Bad packet length descriptor %s")
  , PACKET_BAD_LENGTH("UP-1", "Bad packet length: expected %s, fact %s ")
  , PACKET_BAD_XOR_CHECKSUM("UP-2", "Bad XOR check sum: expected %s, fact %s")
  , PACKET_BAD_ADD_CHECKSUM("UP-3", "Bad ADD check sum: expected %s, fact %s")
  , PACKET_BAD_MASTER_TO_SLAVE("UP-4", "Bad MASTER_TO_SLAVE flag: expected %s, fact %s")
  , PACKET_DATA_OVERHEAD("UP-5", "Data segment is too large: max %s, fact %s")

  , PACKET_REQUEST_TIMEOUT("UR-1", "Request timeout exceeded: %s ms")
  , PACKET_DUPLICATE_REQUEST("UR-2", "Duplicate request id: %s")
  , PACKET_SEND_REQUEST_UNKNOWN_ERROR("UR-3", "unknown error on send request: %s")
  , PACKET_RECEIVE_WITH_INVALID_ID("UR-4", "Receive response with unexpected id: %s")

  , STATION_HAS_NO_SUCCESS_PING("ST-0", "station '%s' (%s) has no success ping")
  , STATION_REQUEST_ERROR("ST-1", "station '%s' (%s) request timed out")

  , STATION_FIND_BY_NAME_ERROR("ST-2", "station with name '%s' does not exists")
  , STATION_FIND_BY_ID_ERROR("ST-3", "station with ID '%s' does not exists")
  , STATION_FIND_BY_ADDRESS_ERROR("ST-4", "station with address '%s' does not exists")
  , STATION_IS_OFF_LINE("ST-5", "station '%s' ('%s') is off line")

  , STATION_DUPLICATE_NAME("ST-6", "station with name '%s' already exists")
  , STATION_DUPLICATE_ID("ST-7", "station with id '%s' already exists")
  , STATION_DUPLICATE_ADDRESS("ST-8", "station with address '%s' already exists")

  , DEVICE_DUPLICATE_NAME("DE-0", "The device with name '%s' exists in remote station '%s'")
  , DEVICE_DUPLICATE_PIN("DE-1", "The device with name '%s' on remote station '%s' already uses the same pin (%s) as the device '%s'")
  , DEVICE_NOT_REGISTERED("DE-2", "The device with name '%s' was not registered on remote station '%s'")
  , DEVICE_IS_NOT_MANAGED("DE-3", "The device with name '%s' at station '%s' is not managed")
  ;

  private SystemError error;
  private String message;
  private String errorCode;

  public boolean isMatchedTo(SystemError error){
    return error.getCode().equals(errorCode);

  }

  public String getErrorCode() {
    return errorCode;
  }

  public SystemError getError(String... args){
    return new SystemError(errorCode, String.format(String.format("%s - %s", errorCode, message), args));
  }

  public void error(String... args){
    throw getError(args);
  }

  SystemErrors(String errorCode, String message){
    this.errorCode = errorCode;
    this.message = message;
  }



}
