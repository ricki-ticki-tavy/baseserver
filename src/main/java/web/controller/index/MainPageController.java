package web.controller.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import web.controller.base.AbstractRestService;

import java.util.Map;

@Controller
public class MainPageController extends AbstractRestService {

  @RequestMapping({"/howToBuy"})
  public String howToBuy(Map<String, Object> model) {
    return "howToBuy";
  }

  @RequestMapping({"/about"})
  public String about(Map<String, Object> model) {
    return "about";
  }

  @RequestMapping({"/promo"})
  public String promo(Map<String, Object> model) {
    return "promo";
  }

  @RequestMapping({"/payAndDelivery"})
  public String payAndDelivery(Map<String, Object> model) {
    return "payAndDelivery";
  }

  @RequestMapping({"/faq"})
  public String faq(Map<String, Object> model) {
    return "faq";
  }

  @RequestMapping({"/orders"})
  public String orders(Map<String, Object> model) {
    return "orders";
  }

  @RequestMapping({"/users"})
  public String users(Map<String, Object> model) {
    return "users";
  }

  @RequestMapping({"/goodAttributes"})
  public String goodAttributes(Map<String, Object> model) {
    return "goodAttributes";
  }

}