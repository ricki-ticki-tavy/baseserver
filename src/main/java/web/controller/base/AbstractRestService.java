package web.controller.base;

import api.core.Result;
import core.system.error.SystemErrors;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * Базовый класс для REST сервисов
 */
public abstract class AbstractRestService {
  protected Result<String> getLoggedinUserName(){
    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    return StringUtils.isEmpty(username)
            ? fail(SystemErrors.SYSTEM_NOT_AUTHORIZED.getError())
            : success(username);
  }
}
