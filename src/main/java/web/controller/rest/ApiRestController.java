package web.controller.rest;
// TODO
// Тесты на dropWeaponByWarrior


import api.core.Result;
import api.core.device.Device;
import api.core.device.RemoteStationManager;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import web.controller.base.AbstractRestService;
import web.controller.base.ResultResponse;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Основной рест-сервер
 */
@RestController
@RequestMapping({"api"})
public class ApiRestController extends AbstractRestService {
  public static final String ADMIN_PREFIX_METHODS = "admin/";
  public static final String USER_PREFIX_METHODS = "user/";

  public static final String METHOD_SET_DEVICE_STATUS = USER_PREFIX_METHODS + "setState";
  public static final String METHOD_GET_GLOBAL_CONFIG_ = USER_PREFIX_METHODS + "getGlobalConfig";

  @Autowired
  RemoteStationManager remoteStationManager;

  @RequestMapping(method = GET, path = {"/" + METHOD_GET_GLOBAL_CONFIG_}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGlobalConfig() {
    return ResultResponse.succeedAsJson(METHOD_GET_GLOBAL_CONFIG_, new GsonBuilder().create().toJson(remoteStationManager.toInfo()));
  }
  //===================================================================================================

  @RequestMapping(method = GET, path = {"/" + METHOD_SET_DEVICE_STATUS}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String setDeviceStatus(@RequestParam(value = "station") String stationName
          , @RequestParam(value = "device") String deviceName
          , @RequestParam(value = "newState") String newState) {

    Result<Device> deviceResult = remoteStationManager.setDeviceState(stationName, deviceName, newState);
    return deviceResult.isSuccess()
            ? ResultResponse.succeedAsJson(METHOD_SET_DEVICE_STATUS, new GsonBuilder().create().toJson(deviceResult.getResult().toInfo()))
            : ResultResponse.failedAsJson(METHOD_SET_DEVICE_STATUS, deviceResult.getError());
  }
  //===================================================================================================


}
