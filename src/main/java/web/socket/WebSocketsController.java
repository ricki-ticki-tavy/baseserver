package web.socket;

import api.core.socket.DeviceStateMessageType;
import api.core.socket.WebSocketDeviceStateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketsController {


  public static final String TOPIC_STATE = "/topic/state";

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  public void sendMessage(DeviceStateMessageType type, Object data) {
    simpMessagingTemplate.convertAndSend(TOPIC_STATE
            , new WebSocketDeviceStateMessage(type, data));
  }
}