function start(adminMode) {
    HOME_CONTROLLER.adminMode = adminMode;
    new HOME_CONTROLLER.ApiRestClient();
    new HOME_CONTROLLER.WebSock();
    new HOME_CONTROLLER.DeviceView();

    HOME_CONTROLLER.deviceView.loadMainView();
}

function switchDevice() {
    curState = !curState;

    HOME_CONTROLLER.apiRestClient.setDeviceState(function () {

        }.bind(this)
        , "западная секция"
        , "Ворота"
        , curState);
}