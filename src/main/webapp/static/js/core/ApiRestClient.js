/**
 * Класс, выполняющий запросы к движку
 * @param externalShowErrorMethod
 * @param lockCursor  - форма-блокиратор UI, для показа во время запросов к серверу
 * @param gameApiUrl
 * @constructor
 */
HOME_CONTROLLER.ApiRestClient = function () {
    HOME_CONTROLLER.apiRestClient = this;
    this.userApiUrl = "api/user/";

    this.managerApi = this.shopApiUrl + "manager/";

    this.lockCursor = new HOME_CONTROLLER.HtmlLockScreenCursor();
};

HOME_CONTROLLER.ApiRestClient.prototype = {
    constructor: HOME_CONTROLLER.ApiRestClient,

    /**
     * Загрузить форму в окно для форм
     * @param formName
     */
    loadForm: function(callback, formName, formData){
        const formContainer = document.getElementById("formContainer");
        // formContainer.innerHTML = "";


        $.ajax({
            url: formName,
            data: formData,
            success: function (response) {
                formContainer.innerHTML = response;
                if (callback) {
                    callback(formName);
                }
            }.bind(this),
            error: this.__showError.bind(this, null),
            dataType: "html"
        })
    },
    //=============================================================================================

    setDeviceState: function(callback, station, device, state){
        this.lockCursor.show();
        $.ajax({
            url: this.userApiUrl + "setState",
            data: "station=" + station + "&device=" + device + "&newState=" + state,
            processData: false,
            contentType: false,
            type: 'GET',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    getGlobalConfig: function(callback){
        this.lockCursor.show();
        $.ajax({
            url: this.userApiUrl + "getGlobalConfig",
            data: "",
            processData: false,
            contentType: false,
            type: 'GET',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузить на сервер изображение
     * @param callback
     * @param formData
     */
    uploadImage: function(callback, formData){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "uploadImage",
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    __successResponse: function(callback, response){
        if (response.failed) {
            this.__externalShowErrorMethod(response.errorMessage, callback.bind(this, response));
            this.lockCursor.hide();
        } else {
            this.lockCursor.hide();
            callback(response);
        }
    },
    //=============================================================================================


    __externalShowErrorMethod: function (message, callback) {
        $.alert(message);
        if (callback) {
            callback(message);
        }
    },
    //============================================================================================


    __showError: function (callback, result) {
        this.lockCursor.hide();
        if (result.statusText != "error" && result.responseText && result.responseText.startsWith("<!DOCTYPE html>")) {
            // это страница логина. Бесполезно париться: переходим на странницу логина
            redirect = function () {
                window.document.location.reload();
            };
            this.__externalShowErrorMethod("Сессия потеряна. Необходима повторная авторизация", redirect);
        } else if (result.statusText == "error") {
            this.__externalShowErrorMethod(result.responseText, callback);
        } else if (result.responseJSON.message) {
            this.__externalShowErrorMethod(result.responseJSON.message, callback);
        } else {
            this.__externalShowErrorMethod(result.responseJSON.error, callback);
        }

    },
    //============================================================================================
};
