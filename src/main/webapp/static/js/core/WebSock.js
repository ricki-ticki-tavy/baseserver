HOME_CONTROLLER.WebSock = function () {
    HOME_CONTROLLER.webSock = this;

    this.WS_INIT_URL = "/wsInit";
    this.stompClient = null;
    this.connected = false;

    this.subscribes = [];
};


HOME_CONTROLLER.WebSock.prototype = {
    constructor: HOME_CONTROLLER.WebSock,

    connect: function (callback) {
        if (this.socket == undefined) {
            this.socket = new SockJS(this.WS_INIT_URL);
            this.stompClient = Stomp.over(this.socket);
        }
        this.stompClient.connect({}, function (frame) {
            if (this.stompClient.connected) {
                // this.setConnected(true);
                console.log('Connected: ' + frame);
                this.stompClient.debug = null;
                this.connected = true;
                this.subscription = this.stompClient.subscribe("/topic/state", callback);
            } else {
                window.setTimeout(this.connect.bind(this, callback), 1000);
                console.log('not connected. retrying a bit later ' + frame);
            }
        }.bind(this));
    },
    //==============================================================================

    disconnect: function () {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        this.connected = false;
        console.log("Disconnected");
    },
    //==============================================================================

    send: function (endPoint, messageObject) {
        this.stompClient.send(endPoint, {}, JSON.stringify(messageObject))
    },
    //==============================================================================
    //==============================================================================
};
