/**
 * Хранит любые настройки, индивидуальные для пользователя
 * @constructor
 */
HOME_CONTROLLER.UserSettingsKeeper = function () {

    HOME_CONTROLLER.userSettingsKeeper = this;

    // настройки
    this.chat = {
        // время жизни сообщений на экране чата арены. секунд
        arenaMessageLiveTimeSec: 10,
    };

    // настройки рендера
    this.render = {
        anisotropy: 16,
        antialias: true,
    };

    this.arena = {
        scale: 0.3,
        wheelCoef: 0.05,
        activeBorders: false,
    };
};

HOME_CONTROLLER.UserSettingsKeeper.prototype = {
    constructor: HOME_CONTROLLER.UserSettingsKeeper,

    load: function () {

    },
};