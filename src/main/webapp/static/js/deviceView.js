HOME_CONTROLLER.DeviceView = function () {
    HOME_CONTROLLER.deviceView = this;

    var connectedLed = undefined;
    var globalConfig = undefined;
    var globalConfig = undefined;

};


HOME_CONTROLLER.DeviceView.prototype = {
    constructor: HOME_CONTROLLER.DeviceView,

    connectedLed: undefined,
    globalConfig: undefined,
    globalConfig: undefined,
    commonStylePart: "position: absolute;",

    __applyConnectedStatus__: function () {
        this.connectedLed.classList.remove("greenStatus");
        this.connectedLed.classList.remove("redStatus");
        this.connectedLed.classList.add(this.globalConfig.connected
            ? "greenStatus"
            : "redStatus");
    },
    //-------------------------------------------------------------------

    __applyDeviceInfo__: function (deviceConfig, stationIsAlive) {
        var led = deviceConfig.deviceLed;
        var style = this.commonStylePart + deviceConfig.uiConfig.commonStyle + ";";
        if (!stationIsAlive || deviceConfig.state == "NULL") {
            led.style = style + deviceConfig.uiConfig.noConnStyle;
        } else {
            switch (deviceConfig.deviceType) {
                case "SWITCHER":
                case "SIMPLE_SWITCHER":
                case "EXTENDED_SWITCHER":
                    if (deviceConfig.state == "true") {
                        led.style = style + deviceConfig.uiConfig.onStyle;
                    } else {
                        led.style = style + deviceConfig.uiConfig.offStyle;
                    }
                    break;
                case "BASE_PROBE":
                    led.innerHTML = deviceConfig.name + "<br>"
                        + deviceConfig.state.toFixed(deviceConfig.analogConfig.accuracy)
                        + " " + deviceConfig.analogConfig.measurement;
                    break;
                case "REGULATOR":
                    break;
            }

        }
    },
    //-------------------------------------------------------------------

    __applyStationInfo__: function (shortStationInfo) {
        var stationConfig;
        if (shortStationInfo.ui == undefined) {
            // short info
            stationConfig = this.globalConfig.remoteStations.find(function (station) {
                return station.name == shortStationInfo.name;
            });
            stationConfig.isAlive = shortStationInfo.isAlive;
        } else {
            // full info
            stationConfig = shortStationInfo;
        }

        if (stationConfig.stationLed != undefined) {
            var style = stationConfig.stationLed.style = this.commonStylePart
                + stationConfig.uiConfig.commonStyle + ";";

            if (stationConfig.isAlive && this.globalConfig.connected) {
                stationConfig.stationLed.style = style + stationConfig.uiConfig.onStyle;
            } else {
                stationConfig.stationLed.style = style + stationConfig.uiConfig.offStyle;
            }
        }

        // apply every device from config
        stationConfig.devices.forEach(function (deviceConfig) {
            // find short info for device
            var deviceShortInfo = shortStationInfo.devices.find(function (shortDeviceInfo1) {
                return shortDeviceInfo1.name == deviceConfig.name
            });
            if (deviceShortInfo != undefined) {
                switch (deviceConfig.deviceType){
                    case "BASE_PROBE":
                        deviceConfig.state = parseFloat(deviceShortInfo.state);
                        break;
                    default:
                        deviceConfig.state = deviceShortInfo.state;
                }
            }
            this.__applyDeviceInfo__(deviceConfig, stationConfig.isAlive);
        }.bind(this));
    },
    //-------------------------------------------------------------------

    __createSwitchDiv__: function (stationConfig, deviceConfig, stationLed) {
        var deviceLed = document.createElement("div");
        deviceConfig.deviceLed = deviceLed;
        deviceLed.classList.add("deviceLed");
        deviceLed.style = this.commonStylePart + deviceConfig.uiConfig.noConnStyle;
        deviceLed.innerHTML = deviceConfig.name;
        deviceLed.id = stationConfig.name + "_" + deviceConfig.name;

        deviceLed.deviceConfig = deviceConfig;
        deviceLed.stationConfig = stationConfig;

        deviceLed.onclick = this.__deviceSwitcherClickHandler.bind(this);

        stationLed.appendChild(deviceLed);

    },
    //---------------------------------------------------------------------

    __createProbeDiv__: function (stationConfig, deviceConfig, stationLed) {
        var probeLed = document.createElement("div");
        deviceConfig.deviceLed = probeLed;
        probeLed.classList.add("deviceLed");
        probeLed.style = this.commonStylePart + deviceConfig.uiConfig.commonStyle + ";" + deviceConfig.uiConfig.noConnStyle;
        probeLed.innerHTML = deviceConfig.name + "<br>-- " + deviceConfig.analogConfig.measurement;
        probeLed.id = stationConfig.name + "_" + deviceConfig.name;

        probeLed.deviceConfig = deviceConfig;
        probeLed.stationConfig = stationConfig;

        probeLed.onclick = this.__deviceSwitcherClickHandler.bind(this);

        stationLed.appendChild(probeLed);

    },
    //---------------------------------------------------------------------

    __createRegulatorDiv__: function (stationConfig, deviceConfig, stationLed) {
        var regulatorLed = document.createElement("div");
        deviceConfig.deviceLed = regulatorLed;
        regulatorLed.classList.add("deviceLed");
        regulatorLed.style = this.commonStylePart + deviceConfig.uiConfig.commonStyle + ";" + deviceConfig.uiConfig.noConnStyle;
        regulatorLed.innerHTML = deviceConfig.name + "<br>-- " + deviceConfig.analogConfig.measurement;
        regulatorLed.id = stationConfig.name + "_" + deviceConfig.name;

        regulatorLed.deviceConfig = deviceConfig;
        regulatorLed.stationConfig = stationConfig;

        var scroller = document.createElement("input");
        scroller.setAttribute("type", "range");
        scroller.setAttribute("min", "0");
        scroller.setAttribute("max", "255");
        scroller.classList.add("scroller");

        regulatorLed.appendChild(scroller);
        stationLed.appendChild(regulatorLed);

    },
    //---------------------------------------------------------------------

    __deviceSwitcherClickHandler: function (clickEvent) {
        var deviceLed = clickEvent.target;
        var deviceConfig = deviceLed.deviceConfig;
        var stationConfig = deviceLed.stationConfig;

        if (stationConfig.isAlive && this.globalConfig.connected) {
            switch (deviceConfig.deviceType) {
                case "SWITCHER":
                case "SIMPLE_SWITCHER":
                case "EXTENDED_SWITCHER":
                    HOME_CONTROLLER.apiRestClient.setDeviceState(function (result) {
                            if (!result.failed) {
                                var newDeviceInfo = JSON.parse(result.result);
                                deviceConfig.state = newDeviceInfo.state;
                                this.__applyDeviceInfo__(deviceConfig, stationConfig);
                            }
                        }.bind(this)
                        , stationConfig.name
                        , deviceConfig.name
                        , (deviceConfig.state == "true" ? "false" : "true"));
                    break;
            }
        } else {
            $.alert("с блоком '" + deviceLed.stationConfig.name + "' нет связт. ");
        }
    },
    //---------------------------------------------------------------------

    loadMainView: function () {
        HOME_CONTROLLER.apiRestClient.getGlobalConfig(function (response) {
            var config = JSON.parse(response.result);
            this.globalConfig = config;
            document.body.innerHTML = "";
            var mainDiv = document.createElement("div");
            mainDiv.classList.add("mainDiv");
            document.body.appendChild(mainDiv);
            var statusDiv = document.createElement("div");
            statusDiv.classList.add("statusDiv");
            statusDiv.innerHTML = "Радиомодуль";
            mainDiv.appendChild(statusDiv);
            this.connectedLed = statusDiv;

            config.remoteStations.forEach(function (stationConfig) {
                if (!stationConfig.uiConfig.noUi) {
                    var stationLed = document.createElement("div");
                    stationLed.classList.add("stationLed");
                    stationLed.style.position = "absolute";
                    stationLed.style.width = stationConfig.uiConfig.width + "mm";
                    stationLed.style.height = stationConfig.uiConfig.height + "mm";
                    stationLed.style.left = stationConfig.uiConfig.left + "mm";
                    stationLed.style.top = stationConfig.uiConfig.top + "mm";
                    stationLed.style.backgroundColor = stationConfig.uiConfig.offlineColor;
                    stationLed.style.border = "solid 1px black";
                    mainDiv.appendChild(stationLed);
                    stationConfig.stationLed = stationLed;
                } else {
                    stationLed = mainDiv;
                }

                stationConfig.devices.forEach(function (deviceConfig) {
                    switch (deviceConfig.deviceType){
                        case "SWITCHER":
                        case "SIMPLE_SWITCHER":
                        case "EXTENDED_SWITCHER":
                            this.__createSwitchDiv__(stationConfig, deviceConfig, stationLed);
                            break;
                        case "BASE_PROBE":
                            this.__createProbeDiv__(stationConfig, deviceConfig, stationLed);
                            break;
                    }

                    this.__resetReloadTimer__();

                    // HOME_CONTROLLER.webSock.subscribeToTopic("/topic/" + this.publicContextTopic, this.__receivePublicEvent__.bind(this));
                    HOME_CONTROLLER.webSock.connect(this.__consumeServerEvent__.bind(this));


                }.bind(this));
            }.bind(this));

            this.__applyConnectedStatus__();
        }.bind(this));
    },
    //--------------------------------------------------------------------

    __resetReloadTimer__: function () {
        if (this.reloadTimeout) {
            window.clearTimeout(this.reloadTimeout);
        }
        this.reloadTimeout = window.setTimeout(function () {
            window.location = window.location;
        }, 10000);
    },

    //--------------------------------------------------------------------

    /**
     * apply new remote station and device state
     * @param stationConfig
     * @private
     */
    __setStationState__: function (shortInfo) {
        this.globalConfig.connected = shortInfo.connected;
        this.__applyConnectedStatus__();
        shortInfo.remoteStations.forEach(function (remoteStation) {
            this.__applyStationInfo__(remoteStation);
        }.bind(this));
    },
    //--------------------------------------------------------------------

    __consumeServerEvent__: function (msg) {
        const message = JSON.parse(msg.body);

        switch (message.messageType) {
            case "PING":
                this.__resetReloadTimer__();
                var shortInfo = JSON.parse(message.messageData);
                this.__setStationState__(shortInfo);
                break;
            case "STATION_STATE_CHANGED":
                break;

        }
    },

};

