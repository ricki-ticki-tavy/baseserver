<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link href="${jstlCss}" rel="stylesheet"/>
    <link href="css/index.css" rel="stylesheet"/>
    <%--<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>--%>
    <%--<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap-responsive.css"/>--%>
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.structure.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.theme.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-confirm.min.css">
    <script src="js/jquery-lib/jquery-3.3.1.js"></script>
    <script src="js/jquery-lib/jquery-ui.js"></script>
    <script src="js/jquery-lib/jquery-confirm.min.js"></script>

    <script src="js/core/defineHomeControllerGlobal.js"></script>
    <script src="js/core/HtmlLockScreenCursor.js"></script>
    <script src="js/core/ApiRestClient.js"></script>
    <script src="js/core/sockjs.js"></script>
    <script src="js/core/stomp.js"></script>
    <script src="js/core/WebSock.js"></script>
    <script src="js/core/mdb.min.js"></script>
    <script src="js/deviceView.js"></script>

    <script src="js/main.js"></script>

    <%--<link href="${jstlCss}" rel="stylesheet"/>--%>
</head>
<body onload="start(true)">

</body>

</html>
